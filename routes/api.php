<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::get("/etudiant/","EtudiantController@getList");
Route::get("/me/encadrant/","EtudiantController@getEncadrant");
Route::get("/me/note/","EtudiantController@getNote");
Route::delete("/me/delete/","EtudiantController@delete");
Route::get("/calendar/","HomeController@calendar");
Route::get("/","HomeController@index");
Route::post("/signup","SignupController@store");

//Dashboard
Route::get("/formIndex","DashboardController@formIndex");
Route::put("/formPut","DashboardController@formPut");

//Report
Route::post("/report","ReportController@store");
Route::get("/report","ReportController@index");
Route::delete("/report","ReportController@delete");
Route::get("/report/get/{groupe}","ReportController@get");
Route::get("/report/download/{uuid}","ReportController@download");



//Form
Route::get("/form","FormController@index");
Route::get("/form/download","FormController@download");

//Reset password

Route::post("/reset","ResetPasswordController@reset");
Route::put("/reset","ResetPasswordController@changePassword");
Route::get("/reset/{token}/{email}","ResetPasswordController@check");

//Admin
Route::post("/admin/auth","admin\AdminController@auth");
Route::post("/admin/auth/me","admin\AdminController@me");


//Encadrant
Route::get("/admin/encadrant/","admin\EncadrantController@getAll");
Route::get("/admin/groupes/","admin\EncadrantController@getAllGroupesAdmin");
Route::post("/admin/groupes/accept/mark","admin\EncadrantController@acceptGroupMark");
Route::get("/admin/encadrant/ms","admin\EncadrantController@myStudentsList");
Route::post("/admin/encadrant/changeMark","admin\EncadrantController@changeMark");
Route::get("/admin/encadrant/teamms","admin\EncadrantController@myTeamStudentsList");

Route::get("/admin/encadrant/team","admin\EncadrantController@jury");
Route::post("/admin/encadrant/","admin\EncadrantController@store");
Route::put("/admin/encadrant/","admin\EncadrantController@affectEnc");
Route::put("/admin/encadrant/update","admin\EncadrantController@update");
Route::get("/admin/encadrant/me","admin\EncadrantController@getConnected");
// Route::get("/admin/encadrant/id/{id}","admin\EncadrantController@getById");
Route::delete("/admin/encadrant/{id}","admin\EncadrantController@remove");
Route::put("/admin/encadrant/reset_s_password/{enc}","admin\EncadrantController@resetPassword");
//soutenance

Route::resource("/admin/soutenance", "admin\SoutenanceController");

//Group by year attestation

Route::get("/admin/attestation","admin\AttestationController@groupeByYear");
Route::get("/admin/attestation/get","admin\AttestationController@getAttestation");
Route::get("/admin/generate_report/{fileire}","admin\AttestationController@generate");

//calendar
Route::resource("/admin/calendar", "admin\CalendarController");



// UsefulDocument
Route::post("/admin/UsefulDocument","admin\UsefulDocumentController@store");
Route::delete("/admin/UsefulDocument/{id}","admin\UsefulDocumentController@destroy");
Route::post("/admin/UsefulDocument/{id}","admin\UsefulDocumentController@update");
Route::get("/admin/UsefulDocument","admin\UsefulDocumentController@index");


//Admin settings
Route::get("/settings","admin\AdminSettingsController@index");
Route::get("/admin/statistics","admin\AdminController@statistics");
Route::put("/admin/settings","admin\AdminSettingsController@update");
Route::put("/admin/settings/password","admin\AdminSettingsController@changePassword");


//Jury
Route::get("/admin/jury/list/","admin\JuryController@list");
Route::post("/admin/jury/create/","admin\JuryController@create");
Route::put("/admin/jury/assignEncadrant/{encadrant}/{jury}","admin\JuryController@assignEncadrant");
Route::delete("/admin/jury/remove/{jury}","admin\JuryController@delete");
Route::delete("/admin/jury/removeEnc/{encadrant}","admin\JuryController@removeEncadrant");

Route::get("/redis","RedisController@send");
