<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table("admins")->insert([
            "username"=>"admin",
            "password"=>bcrypt("admin2020"),
            "encadrant_id"=>null
        ]);
    }
}
