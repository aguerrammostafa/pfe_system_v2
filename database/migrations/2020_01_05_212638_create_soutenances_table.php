<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoutenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soutenances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('at_date');
            $table->string('at_time');
            $table->unsignedBigInteger('groupe_id');
            $table->string('salle')->nullable();
            $table->unsignedBigInteger('jury_1');
            $table->unsignedBigInteger('jury_2')->nullable();
            $table->unsignedBigInteger('jury_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soutenances');
    }
}
