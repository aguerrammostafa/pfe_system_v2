<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Group extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groupes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique();
            $table->string('password');
            $table->string("email");
            $table->string("filiere");
            $table->integer("encadrant_id")->default(0);
            $table->string("sujet")->nullable();
            $table->text("desc")->nullable();
            $table->string("mots_cle")->nullable();

            $table->date("debut_stage")->nullable();
            $table->date("fin_stage")->nullable();
            $table->unsignedInteger("duree_stage")->nullable();
            $table->unsignedInteger("pre_embauche")->default(0);

            $table->date("date_consult")->nullable();;
            $table->unsignedInteger("rapport")->nullable();
            $table->boolean("valide")->default(false);
            $table->float("note")->nullable();


            $table->string("entreprise")->nullable();
            $table->string("ville_entreprise")->nullable();
            $table->text("adresse_entreprise")->nullable();
            $table->text("coordonnes_entreprise")->nullable();
            $table->string("nom_encadrant_ext")->nullable();
            $table->string("fonction_encadrant_ext")->nullable();
            $table->string("email_encadrant_ext")->nullable();
            $table->string("gsm_encadrant_ext")->nullable();
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groupes');
    }
}
