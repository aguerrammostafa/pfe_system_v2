<?php

namespace App;

use DateTime;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Groupe extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $guarded = ["id"];
    protected $hidden = [
        "password",
        "remember_token",
        "email_verified_at",
    ];

    public function etudiants()
    {
        return $this->hasMany('App\Etudiant');
    }

    public function encadrant()
    {
        return $this->belongsTo('App\Encadrant');
    }

    public function allReports()
    {
        return $this->hasMany("App\Report");
    }
    public function activeReport()
    {
        return $this->hasOne("App\Report")
            ->orderBy("id","desc")
            ->where("isFinal","=",0)
            ->orWhere("isFinal","=",1);
    }
    public function activate()
    {
        $this->setRememberToken(null);
        $this->update();
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function username()
    {
        return 'email';
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    public function setDebutStageAttribute($value)
    {
        try{
            $date = new DateTime($value);
            $this->attributes['debut_stage'] = $date->format("Y-m-d");
        }
        catch (\Exception $ex)
        {
            log($ex);
        }
    }
    public function setVilleEntrepriseAttribute($value){
        $this->attributes["ville_entreprise"] = ucfirst($value);
    }
    public function setNomEncadrantExtAttribute($value){
        $this->attributes["nom_encadrant_ext"] = ucwords($value);
    }

}
