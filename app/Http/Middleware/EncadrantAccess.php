<?php

namespace App\Http\Middleware;

use Closure;


class EncadrantAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\auth("admin")->check())
        {
            if(\auth("admin")->user()->encadrant_id != null)
            {
                return $next($request);
            }
        }
        return response()->json([
            "message"=>"Vous n'avez pas les droits d'accès"
        ],401);
    }
}
