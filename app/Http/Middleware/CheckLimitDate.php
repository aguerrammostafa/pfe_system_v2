<?php

namespace App\Http\Middleware;

use App\DatesSetting;
use App\Resources\DateLimitTypes;
use Closure;
use Exception;
class CheckLimitDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$type)
    {
        try{
            $res = DatesSetting::where("title",$type)->first();
            if($res)
            {
                if(strtotime($res->at) >= strtotime(date("Y-m-d")))
                {
                    return $next($request);
                }
                else{
                    return response()->json([
                        "message"=>"La date limité est dépassé"
                    ],400);
                }
            }
            else{
                throw new \Exception("$type not found as a date limit");
            }
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }
}
