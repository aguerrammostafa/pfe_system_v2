<?php

namespace App\Http\Controllers;

use App\Calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    function index(Request $request)
    {
        return Response::json([
            "message" => "Hello"
        ]);
    }

    function calendar()
    {
        return Calendar::orderBy("at_date", "asc")->get();
    }
}
