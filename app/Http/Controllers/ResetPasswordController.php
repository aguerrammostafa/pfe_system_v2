<?php

namespace App\Http\Controllers;

use App\Events\EtudiantPasswordResetEvent;
use App\Groupe;
use App\Jobs\EtudnaitPasswordResetJob;
use App\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{

    public function changePassword(Request $request)
    {
        $request->validate([
            "password" => "min:8|max:255|required_with:r_password|same:r_password",
            "r_password" => "required"
        ]);
        $token = $request->token;
        $email = $request->email;
        if (isset($token) && isset($email)) {
            $pr = PasswordReset::where("token", $token)->first();
            if ($pr == null || ($pr->groupe() != null && $pr->groupe()->email != $email)) {
                return response()->json([
                    "message" => "Erruer"
                ], 400);
            }
            PasswordReset::where("token",$token)->delete();

            $grp = Groupe::where("email",$email)->first();
            $grp->setPasswordAttribute($request->password);
            $grp->save();

            return [
                "message"=>"Votre mot de passe a été mis à jour avec succès"
            ];
        }
        else{
            return response()->json([
                "message"=>"Erruer"
            ],401);
        }
    }

    public function check(Request $request, $token, $email)
    {
        $pr = PasswordReset::where("token", $token)->first();
        if ($pr == null || ($pr->groupe() != null && $pr->groupe()->email != $email)) {
            return response()->json([
                "message" => "Erruer"
            ], 400);
        }
        return [
            "message" => "ok"
        ];
    }

    public function reset(Request $request)
    {
        $request->validate([
            "email" => "required"
        ]);
        $grp = Groupe::where("email", $request->email)->first();
        if ($grp == null) {
            return response()->json([
                "message" => "Email n'existe pas"
            ], 400);
        } else {

            $token = Str::random(32);
            $email = $grp->email;

            $pr = PasswordReset::create([
                "token" => $token,
                "email" => $email
            ]);
//            $this->dispatch(new EtudnaitPasswordResetJob($pr));
            event(new EtudiantPasswordResetEvent($pr));
            return [
                "message" => "Merci de consulter votre email"
            ];
        }
    }
}
