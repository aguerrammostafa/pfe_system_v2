<?php

namespace App\Http\Controllers;

use App\Groupe;
use App\Report;
use Cassandra\Exception\UnauthorizedException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Resources\DateLimitTypes;
class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:api"])->except(["get","download"]);
        // $this->middleware(["limit_date:".DateLimitTypes::$DATE_LIMIT_COMPLETE_DONNESS])->only(["store","delete"]);
    }

    public function get(Request $request, Groupe $groupe)
    {
        return $groupe->activeReport()->select("groupe_id","find","id","title","isFinal","created_at")->first();
    }

    public function index(Request $request)
    {
        return auth()->user()->allReports()->select("groupe_id","find","id","title","isFinal","created_at")->orderBy("id","desc")->get();
    }
    public function store(Request $request)
    {
        $request->validate([
           "file"=>["required","file","mimes:pdf","max:".config("app.report_max_size")],
           "isFinal"=>"required|boolean"
        ]);

        $file =  $request->file("file");
        $id = auth()->user()->id;
        $title = "Groupe-".$id."-".time().".pdf";
        $link = "";

        try{
            $link = Storage::putFileAs(
                'reports/groupe'.$id,
                $file,
                $title
            );
            $oldOn = Report::where([
                "groupe_id"=>$id,
                "isFinal"=>$request->isFinal
            ])->first();
            if($oldOn)
            {
                $oldOn->delete();
            }
            $report = Report::updateOrCreate([
                "groupe_id"=>$id,
                "isFinal"=>$request->isFinal
            ],[
                "link"=>$link,
                "title"=>$title,
                "find"=>Str::uuid()
            ]);

            /*$current_one = auth()->user()->activeReport()->first();
            if($current_one)
            {
                $current_one->active = false;
                $current_one->save();
            }*/

            //$report->save();
            return [];
        }
        catch(\Exception $ex)
        {
            Storage::delete($link);
            return response()->json([
                "message"=>"Quelque chose a mal tourné : ".$ex->getMessage()
            ],422);
        }
        return [
            "message"=>"Votre rapport a été téléchargé avec succès."
        ];
    }
    public function delete(Request $request)
    {
        if($request->id)
        {
            $report = Report::findOrFail($request->id);
            if($report->groupe_id == auth()->user()->id)
            {
                $report->delete();
                return [];
            }
            else{
                throw new UnauthorizedException();
            }
        }
    }
    public function download(Request $request,$uuid)
    {
        $report = Report::where("find","=",$uuid)->first();
        if($report)
        {
            return response()->download(storage_path("/app/".$report->link));
        }
        else{
            return response()->json(["message"=>"Not found"],404);
        }
    }
}
