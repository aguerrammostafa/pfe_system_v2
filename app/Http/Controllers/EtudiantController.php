<?php

namespace App\Http\Controllers;

use App\Etudiant;
use App\Groupe;
use Illuminate\Http\Request;

class EtudiantController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:api"])->except(["getList"]);
    }
    public function getList(Request $request)
    {
        //TODO: fix this to return only wanted fields not all of them
        $list = Groupe::select(["id","filiere","sujet","desc","ville_entreprise","encadrant_id"])
            ->where("remember_token",null)
            ->with(
                [
                    "etudiants:id,groupe_id,nom,prenom",
                    "encadrant:id,groupe_id,nom,prenom"
                ])->get();
        return response()->json($list);
    }
    public function getEncadrant(Request $request)
    {
        return auth()->user()->encadrant()->first();
    }
    public function delete(Request $request)
    {
        $groupe = auth()->user();
        $repos = $groupe->allReports()->get();
        $etudiant = $groupe->etudiants()->get();
        foreach($repos as $r)
        {
            $r->delete();
        }
        foreach($etudiant as $r)
        {
            $r->delete();
        }
        $groupe->delete();
        return [];
    }
    public function getNote()
    {
        $groupe = auth()->user();
        return Groupe::where([
            "id"=>$groupe->id,
            "valide"=>"1",
            "note_verified"=>"1"
        ])->select(["note","remark"])->first();
    }
}
