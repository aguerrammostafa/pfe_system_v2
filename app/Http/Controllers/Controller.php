<?php

namespace App\Http\Controllers;

use App\Services\RedisService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redis;
/**
 * @author Mostafa Aguerram
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * function to store a value with a key in redis
     * @param string key
     * @param mixed value
     */

    public function redisSet($key,$value)
    {
        $val = $value;
        if(is_array($value))
        {
            $val = json_encode($value);
        }
        Redis::set($key,$val);
    }
    /**
     * function to get a value of a key from redis
     * @return found value or null of there isn't any
     *
     */
    public function redisGet($key)
    {
        return Redis::get($key);
    }
    /**
     * function to publish value to a channel
     * NOTE: there should be a subscriber script for that channel or the value will be lost
     * @param string channel channel where to publish the value
     * @param mixed value to publish
     */
    public function redisPublish($channel,$value)
    {
        $val = $value;
        if(is_array($value))
        {
            $val = json_encode($value);
        }
        Redis::publish($channel,$val);
    }
}
