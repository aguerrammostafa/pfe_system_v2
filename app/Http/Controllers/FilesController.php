<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilesController extends Controller
{
    public function download($file)
    {
        $path = storage_path("/app/files/$file");
        if(file_exists($path))
        {
            return response()->download($path);
        }
        return redirect("/nofound");
    }
}
