<?php

namespace App\Http\Controllers;

class RedisController extends Controller
{
    public function send()
    {
        $this->redisPublish("log",json_encode([
            "user_id"=>0,
            "url"=>"/api/admin/add",
            "at"=>time()
        ]) );
        return [];
    }
}
