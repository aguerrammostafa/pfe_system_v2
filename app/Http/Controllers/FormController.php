<?php

namespace App\Http\Controllers;

use App\Groupe;
use PDF;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:api"]);
    }

    private function data()
    {
        $id = auth()->user()->id;
        $data = Groupe::where("id",$id)->with("etudiants", "encadrant")->first();
        $data['logo'] = base64_encode(file_get_contents(resource_path("img/ensa_logo.png")));
        $data['filiere'] = $data['filiere'] == "GI" ? "Génie Informatique" : "Génie Télécom et Réseaux";
        return $data;
    }
    public function index(Request $request)
    {
        $data = $this->data();
        return view("form.pdf", $data);
    }
    public function download(Request $request)
    {
        $data = $this->data();
        $pdf = PDF::loadView('form.pdf', $data);
        return $pdf->download();
    }
}
