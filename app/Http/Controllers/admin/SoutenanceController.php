<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Soutenance;
use Illuminate\Http\Request;

class SoutenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Soutenance::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "at_date"=>"date|required",
            "at_time"=>"string|required|max:20",
            "groupe_id"=>"integer|required",
            "salle"=>"string|max:30|required",
            "jury_1"=>"integer|required",
            "jury_2"=>"integer",
            "jury_3"=>"integer",
        ]);

        Soutenance::create([
            "at_date"=> $request->at_date,
            "at_time"=> $request->at_time,
            "groupe_id"=>$request->groupe_id,
            "salle"=>$request->salle,
            "jury_1"=>$request->jury_1,
            "jury_2"=>$request->jury_2,
            "jury_3"=>$request->jury_3,
        ]);
        return ["message"=>"Soutenance planifiée avec succès."];

    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "at_date" => "date|required",
            "at_time" => "string|required|max:20",
            "groupe_id" => "integer|required",
            "salle" => "string|max:30|required",
            "jury_1" => "integer|required",
            "jury_2" => "integer",
            "jury_3" => "integer",
        ]);

        $soutenace = Soutenance::find($id);
        $soutenace->at_date = $request->at_date;
        $soutenace->at_time = $request->at_time;
        $soutenace->groupe_id = $request->groupe_id;
        $soutenace->salle = $request->salle;
        $soutenace->jury_1 = $request->jury_1;
        $soutenace->jury_2 = $request->jury_1;
        $soutenace->jury_3 = $request->jury_3;
        $soutenace->save();
        return ["message" => "Planning de soutenance mis à jour avec succès."];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Soutenance::destroy([
            "id"=>$id
        ]);
        return ["message"=> "Planning de soutenance supprimé avec succès."];
    }
}
