<?php

namespace App\Http\Controllers\admin;

use App\Admin;
use App\Groupe;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Encadrant;
use App\Jury;

class EncadrantController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:admin"]);
        $this->middleware(["adminAccess"])->except(["myTeamStudentsList", "getAll", "getConnected", "getById", "update", "jury", "myStudentsList", "changeMark"]);
        $this->middleware(["encadrantAccess"])->only(["myTeamStudentsList", "update", "getConnected", "jury", "myStudentsList", "changeMark"]);
    }

    public function getAllGroupesAdmin()
    {
        return Groupe::with("encadrant", "etudiants")->get();
    }

    public function acceptGroupMark(Request $request)
    {
        $request->validate([
            "note" => "numeric|min:0|max:20",
            "remark" => "string",
            "groupe" => "numeric"
        ]);

        $grp = Groupe::where("id", $request->groupe)->first();
        if ($grp == null) {
            return response()->json([], 404);
        }

        $grp->note = $request->note;
        $grp->remark = $request->remark;
        $grp->note_verified = true;
        $grp->valide=true;
        $grp->save();
        return [];
    }

    public function getAll()
    {
        return Encadrant::select()
            ->withCount(["groupes as encadrer"])
            ->with("admin:id,encadrant_id,username")
            ->orderBy("nom", "asc")
            ->orderBy("prenom", "asc")
            ->get();
    }

    public function resetPassword(Encadrant $enc)
    {
        $account = Admin::where("encadrant_id", $enc->id)->first();
        $account->setPasswordAttribute("123456789");
        $account->save();
        return [];
    }

    public function affectEnc(Request $request)
    {
        $list = $request->list;
        foreach ($list as $el) {
            $grp = Groupe::where("id", $el['groupe_id'])->first();
            $grp->encadrant_id = $el['supervisor'];
            $grp->save();
        }
        return [];
    }

    public function store(Request $request)
    {
        $request->validate([
            "username" => "required|min:2|unique:admins",
            "password" => ["required", "min:8", 'regex:' . config("app.username_regex")],
            "nom" => "required|min:2|regex:/^[a-zA-Z\s\-\']*$/",
            "prenom" => "required|min:2|regex:/^[a-zA-Z\s\-\']*$/",
            "email" => "required|email",
            "gsm" => ["required", "regex:" . config("app.phone_regex")]
        ]);


        $encd = Encadrant::create([
            "nom" => strtoupper($request->nom),
            "prenom" => ucfirst($request->prenom),
            "email" => strtolower($request->email),
            "gsm" => $request->gsm
        ]);
        Admin::create([
            "username" => strtolower($request->username),
            "password" => $request->password,
            "encadrant_id" => $encd->id
        ]);

        return [];
    }

    public function getConnected()
    {
        return Encadrant::where("id", auth()->user()->encadrant_id)->first();
    }

    public function getById($id)
    {
        return Encadrant::find($id);
    }

    public function update(Request $request)
    {
        $request->validate([
            "password" => ["nullable", "min:8", 'regex:' . config("app.username_regex")],
            "nom" => "required|min:2|regex:/^[a-zA-Z\s\-\']*$/",
            "prenom" => "required|min:2|regex:/^[a-zA-Z\s\-\']*$/",
            "fonction" => "nullable",
            "email" => "required|email",
            "gsm" => ["required", "regex:" . config("app.phone_regex")]
        ]);
        $encadrant = $this->getConnected();
        if ($request->password) {
            $user = auth()->user();
            $user->setPasswordAttribute($request->password);
            $user->save();
        }
        $encadrant->nom = $request->nom;
        $encadrant->prenom = $request->prenom;
        $encadrant->email = $request->email;
        $encadrant->gsm = $request->gsm;
        $encadrant->fonction = $request->fonction;
        $encadrant->save();
        return [];
    }

    public function remove($id)
    {
        $encadrant = $this->getById($id);
        $encadrant->delete();
    }

    public function jury()
    {
        $jury = $this->getConnected()->jury();
        if ($jury) {
            return $jury->encadrants($this->getConnected()->id)->get();
        } else {
            return -1;
        }
    }

    public function changeMark(Request $request)
    {
        $request->validate([
            "note" => "numeric|min:0|max:20",
            "remark" => "string",
            "groupe" => "numeric"
        ]);
        $encadrant = $this->getConnected();
        $grp = Groupe::where("id", $request->groupe)->first();
        if ($grp == null) {
            return response()->json([], 404);
        }
        if ($grp->encadrant === $encadrant) {
            return response()->json([], 401);
        }

        $grp->note = $request->note;
        $grp->remark = $request->remark;
        $grp->note_verified = false;
        $grp->valide=true;
        $grp->save();
        return [];
    }

    public function myStudentsList()
    {
        $encadrant = $this->getConnected();
        return $encadrant->groupesShort()->with(["etudiants", "allReports", "encadrant"])->get();
    }

    public function myTeamStudentsList()
    {
        $jury = $this->jury();
        if (!is_numeric($jury)) {
            $slist = [];
            foreach ($jury as $enc) {
                array_push($slist, $enc->groupesShort()->with(["etudiants", "allReports", "encadrant"])->get());
            }
            //$encadrant = $this->getConnected();
            return $slist;
        }
        return -1;

    }
}
