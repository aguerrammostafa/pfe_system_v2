<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Groupe;
use Illuminate\Http\Request;
use PDF;

class GenerateReportController extends Controller
{
    public function fileire(Request $request,$fileire)
    {
        $groupe = Groupe::where("filiere",$fileire)->with("etudiants","encadrant");
        $arr = $groupe;
//        $pdf = PDF::loadView('form.attestation', ["data"=>$arr]);

//        return $pdf->download("Fileire-"+$fileire+".pdf");
        return view("form.fileire", ["data"=>$arr]);
    }
}
