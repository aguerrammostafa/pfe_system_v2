<?php

namespace App\Http\Controllers\admin;

use App\Encadrant;
use App\Http\Controllers\Controller;
use App\Jury;
use Illuminate\Http\Request;

class JuryController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:admin"]);
        $this->middleware(["adminAccess"]);
    }
    public function list()
    {
        $v = Jury::with([
            "encadrants" => function ($db) {
                return $db->select(["id", "jury", "nom", "prenom"])->withCount("groupes");
            }
        ])->get()->toArray();
        $d = \App\Encadrant::where("jury", 0)->select(["id", "nom", "prenom"])->withCount("groupes")->get()->toArray();
        $d = array_merge([
            [
                "id" => 0,
                "encadrants" => $d
            ]
            ],$v);
        return $d;
    }
    public function assignEncadrant(Encadrant $encadrant, Jury $jury)
    {
        $encadrant->jury = $jury->id;
        $encadrant->save();
        return [];
    }
    public function removeEncadrant(Encadrant $encadrant)
    {
        $encadrant->jury = 0;
        $encadrant->save();
        return [];
    }
    public function create(Request $request)
    {
        $title = "";
        if (isset($request->title)) {
            $title = $request->title;
        }
        Jury::create([
            "title" => $title
        ]);
        return [];
    }
    public function delete(Jury $jury)
    {
        $jury->delete();
        return [];
    }
}
