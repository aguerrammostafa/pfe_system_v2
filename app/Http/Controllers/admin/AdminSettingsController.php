<?php

namespace App\Http\Controllers\admin;

use App\Admin;
use App\DatesSetting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Resources\DateLimitTypes;

class AdminSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:admin", "adminAccess"])->except("index");
    }

    /*
        date_pre
        date_com_don
        date_ins_rap
    */
    public function index(Request $request)
    {
        $dates = DatesSetting::all();
        $data = [
            "version" => 1
        ];

        foreach ($dates as $val) {
            $data[$val->title] = $val->at;
        }
        return $data;
    }
    public function update(Request $request)
    {
        $list = [
            DateLimitTypes::$DATE_LIMIT_COMPLETE_DONNESS,
            DateLimitTypes::$DATE_LIMIT_FIRST_REPORT,
            DateLimitTypes::$DATE_LIMIT_INSCREPTION
        ];

        $validation = [];
        foreach ($list as $el) {
            $validation = array_merge($validation, [
                $el => "required|date"
            ]);
        }

        $request->validate($validation);

        foreach ($list as $element) {
            $model = DatesSetting::where("title", $element)->first();
            if ($model) {
                $model->at = $request->get($element);
                $model->save();
            } else {
                DatesSetting::create([
                    "title" => $element,
                    "at" => $request->get($element)
                ]);
            }
        }

        return [];
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            "current_password" => "required",
            "new_password" => "required|min:8"
        ]);

        $admin = Admin::where("id", auth()->user()->id)->first();
        if ($admin->comparePassword($request->current_password)) {
            $admin->password = $request->new_password;
            $admin->save();
            return [];
        } else {
            return response()->json([
                "message" => "Le mot de passe actuel est incorrect"
            ], 400);
        }
    }
}
