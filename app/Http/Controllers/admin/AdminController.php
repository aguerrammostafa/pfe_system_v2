<?php

namespace App\Http\Controllers\admin;

use App\Encadrant;
use App\Etudiant;
use App\Groupe;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['auth']]);
        $this->middleware("adminAccess")->only("statistics");
    }

    public function me()
    {
        return response()->json([
            "message" => "Ok"
        ]);
    }

    public function auth(Request $request)
    {
        $request->validate([
            "username" => "required",
            "password" => "required"
        ]);
        $credentials = request(['username', 'password']);
        if (!$token = auth("admin")->attempt($credentials)) {
            return response()->json(['message' => 'Votre nom d\'utilisateur ou mot de passe est incorrect'], 401);
        }
        $user = auth('admin')->user();
        return $this->respondWithToken($token, $user->encadrant_id == null);
    }

    protected function respondWithToken($token, $admin)
    {
        return response()->json([
            'access_token' => $token,
            "admin" => $admin
        ]);
    }

    public function statistics(Request $request)
    {
        return [
            "etudiants" => Etudiant::select(["id", "groupe_id"])->with(["groupe:id,filiere"])->get(),
            "encadrants" => Encadrant::all()->count(),
            "groupes" => Groupe::groupBy("filiere")->select("filiere", DB::raw("count(*) as total"))->where("remember_token", null)->get(),
            "groupes_non" => Groupe::groupBy("filiere")->select("filiere", DB::raw("count(*) as total"))->where("remember_token", "!=", null)->get(),
            "notes_not_verified" => Groupe::groupBy("filiere")
                ->select("filiere", DB::raw("count(*) as totle"))
                ->where([
                    "valide" => "1",
                    "note_verified" => "0"
                ])->get(),
            "notes_not_set" =>
                Groupe::groupBy("filiere")
                    ->select("filiere", DB::raw("count(*) as totle"))
                    ->where([
                        "valide" => "0"
                    ])->get(),
            "note_avg" => Groupe::groupBy("filiere")
                ->select("filiere", DB::raw("avg(note) as avg"))
                ->where([
                    "valide" => "1",
                    "note_verified" => "1"
                ])->get()
        ];
    }
}
