<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Calendar;
use Illuminate\Http\Request;

class CalendarController extends Controller
{

    public function __construct()
    {
        $this->middleware(["auth:admin","adminAccess"])->except("index");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Calendar::orderBy("at_date","asc")->get();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "description"=>"required|string|max:1000",
            "at_date"=>"required|date"
        ]);

        $calendar = Calendar::find($id);
        if($calendar)
        {
            $calendar->description = $request->description;
            $calendar->at_date = $request->at_date;
            $calendar->save();
        }
        return ["message"=>"Entrée mise à jour avec succès"];
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "description"=>"required|string|max:1000",
            "at_date"=>"required|date"
        ]);

        Calendar::create([
            "at_date"=>$request->at_date,
            "description"=>$request->description
        ]);
        return ["message"=>"Entrée enregistrée avec succès."];
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Calendar::destroy([
            "id"=>$id
        ]);
        return ["message"=>"Entrée supprimée avec succès."];
    }
}
