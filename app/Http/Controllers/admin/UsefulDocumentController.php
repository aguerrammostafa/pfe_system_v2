<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\UsefulDocument;

class UsefulDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware(["auth:admin"])->except("index");
    }

    public function index()
    {
        // get all the documents
        $usefulDocuments = UsefulDocument::all();
        return $usefulDocuments;

    }
    public function update(Request $request,$id){
        $request->validate([
            "file"=>["mimes:pdf","max:".config("app.report_max_size")],
         ]);
        try{
            $usefulDocument = UsefulDocument::findOrFail($id);
            $usefulDocument->title = $request->get("title").".pdf";
            $usefulDocument->description = $request->get("description");
            $link = "";
            if($request->file("file")){
                $file=  $request->file("file");
                Storage::delete($usefulDocument->link);
                $link = Storage::putFileAs(
                    'UsefulDocuments',
                    $file,
                    $usefulDocument->title,
                );
                $usefulDocument->link = $link;
            }
            $usefulDocument->save();

        }catch(\Exception $e){
            return response()->json([
                "message"=>"ce fichier n'existe pas dans la base de données"
            ],422);
        }
        return [
            "message"=>"Votre Document a bien été mis á jour"
        ];
    }

    public function store(Request $request)
    {

        $request->validate([
           "file"=>["required","file","mimes:pdf","max:".config("app.report_max_size")],
           "title"=>["required"]
        ]);

        $file =  $request->file("file");
        $title = $request->get("title").".pdf";
        $description = $request->get("description");
        $link = "";
        try{
            $link = Storage::putFileAs(
                'UsefulDocuments',
                $file,
                $title
            );
            $uploadedDocument = new UsefulDocument([
                "link"=>$link,
                "title"=>$title,
                "description"=>$description,
                "find"=>Str::uuid()
            ]);

            $uploadedDocument->save();
        }
        catch(\Exception $ex)
        {
            Storage::delete($link);
            return response()->json([
                "message"=>"Quelque chose a mal tourné : ".$ex->getMessage()
            ],422);
        }
        return [
            "message"=>"Votre Document a bien été publié"
        ];
    }
    public function destroy($id)
    {
        try{
            $usefulDocument = UsefulDocument::findOrFail($id);
            // Storage::delete($usefulDocument->link);
            $usefulDocument->delete();
        }catch(ModelNotFoundException $e){
            return response()->json([
                "message"=>"ce fichier n'existe pas dans la base de données"
            ],422);
        }
        return [
            "message"=>"Votre Document a bien été supprimé"
        ];
    }
}
