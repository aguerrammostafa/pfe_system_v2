<?php

namespace App\Http\Controllers\admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Groupe;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttestationController extends Controller
{
    public function __construct()
    {
        $this->middleware(["auth:admin","encadrantAccess"])->except("index");
    }

    public function groupeByYear(Request $request)
    {
        $id =  auth()->user()->id;
        $encadrant = Admin::where("id",$id)->first()->encadrant();
        $groupes = $encadrant->groupes()->with("etudiants")->get()->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('Y');
        });


        $arr = [];
        foreach ($groupes as $key=>$grp)
        {
            array_push($arr,[
                "year"=>$key,
                "data"=>$grp
            ]);
        }

        return $arr;
    }
    public function getAttestation(Request $request)
    {

        $id =  auth()->user()->id;
        $groupes = auth()->user()->groupes()->with("etudiants")->get()->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('Y');
        });

        $arr = [];
        foreach ($groupes as $key=>$grp)
        {
            array_push($arr,[
                "year"=>$key,
                "data"=>$grp
            ]);
        }

        $arr["prof_name"] = auth()->user()->nom." ".auth()->user()->prenom;
        $pdf = PDF::loadView('form.attestation', ["data"=>$arr]);

        return $pdf->download("Attestation.pdf");
//        return view("form.attestation", ["data"=>$arr]);
    }
}
