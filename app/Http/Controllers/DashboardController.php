<?php

namespace App\Http\Controllers;

use App\DatesSetting;
use App\Etudiant;
use App\Groupe;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use App\Resources\DateLimitTypes;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        // $this->middleware(["limit_date:" . DateLimitTypes::$DATE_LIMIT_COMPLETE_DONNESS])->only(["formPut"]);
    }

    public function formIndex(Request $request)
    {
        $id = auth()->user()->id;
        $item = Groupe::where('id', $id)
            ->select(
                [
                    'id',
                    'email',
                    "filiere",
                    "username",
                    "sujet",
                    "desc",
                    "debut_stage",
                    "duree_stage",
                    "mots_cle",
                    "pre_embauche",
                    "entreprise",
                    "ville_entreprise",
                    "coordonnes_entreprise",
                    "nom_encadrant_ext",
                    "fonction_encadrant_ext",
                    "email_encadrant_ext",
                    "gsm_encadrant_ext",
                ]
            )
            ->with(['etudiants' => function ($query) { //etudiants:id,groupe_id,....
                $query->select(['id', 'groupe_id', 'nom', "prenom", "email", "gsm"]);
            }])->first();
        $item['total_members'] = count($item['etudiants']);
        $item['version'] = 1;
        $item["list"] = $item['etudiants'];
        unset($item['etudiants']);
        return $item;
    }

    public function formPut(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            "username" => ['required', 'string', 'max:255', "min:4", 'unique:groupes,id,' . $user->id, 'regex:' . config("app.username_regex")],
            "password" => "nullable|min:8|max:255|required_with:r_password|same:r_password",
            "r_password" => "nullable",
            "filiere" => "required|in:GTR,GI",
            "list" => "required|array|min:1|max:" . config("app.max_students"),
            "list.*.nom" => "required|max:255|min:2|regex:/^[a-zA-Z\s\-\']*$/",
            "list.*.prenom" => "required|max:255|min:2|regex:/^[a-zA-Z\s\-\']*$/",
            "list.*.email" => "required|email|unique:groupes,id," . $user->id,
            "list.*.gsm" => ["required", "regex:" . config("app.phone_regex")],
            "sujet" => "nullable|max:255",
            "desc" => "nullable|max:3000",
            "debut_stage" => "nullable|date",
            "mots_cle" => "nullable|string|max:255",
            "duree_stage" => "nullable|integer|min:0|max:760",
            "pre_embauche" => "required|min:0|max:2",
            "entreprise" => "nullable|max:255",
            "ville_entreprise" => "nullable|max:255",
            "coordonnes_entreprise" => "nullable|max:3000",
            "nom_encadrant_ext" => "nullable|max:255",
            "fonction_encadrant_ext" => "nullable|max:255",
            "email_encadrant_ext" => "nullable|max:255|email",
            "gsm_encadrant_ext" => ["nullable", "regex:" . config("app.phone_regex")],
        ]);
        try {
            $list = $request->list;
            $group = Groupe::find($user->id);

            $res = DatesSetting::where("title", DateLimitTypes::$DATE_LIMIT_COMPLETE_DONNESS)->first();
            if ($res && (strtotime($res->at) >= strtotime(date("Y-m-d")))) {
                $group->username = $request->username;
                $group->email = strtolower($request->list[0]["email"]);

                $group->sujet = $request->sujet;
                $group->entreprise = $request->entreprise;
                $group->gsm_encadrant_ext = $request->gsm_encadrant_ext;
                $group->email_encadrant_ext = $request->email_encadrant_ext;
                $group->fonction_encadrant_ext = $request->fonction_encadrant_ext;
                $group->nom_encadrant_ext = $request->nom_encadrant_ext;
                $group->coordonnes_entreprise = $request->coordonnes_entreprise;
                $group->ville_entreprise = $request->ville_entreprise;
                $group->pre_embauche = $request->pre_embauche;
                $group->mots_cle = $request->mots_cle;
                $group->desc = $request->desc;
                $group->desc = $request->desc;
                $group->duree_stage = $request->duree_stage;
                $group->debut_stage = $request->debut_stage;
                $group->filiere = $request->filiere;
            }
            if ($request->password)
                $group->password = $request->password;
            $group->save();

            $students = $group->etudiants;
            $arr = [];
            //foreach ($)
            //update first existing ones
            $addition = 0;
            $delete_list = [];
            foreach ($students as $std) {
                $i = -1 + $addition;
                $found = false;
                foreach ($list as $el) {
                    $i++;
                    if (isset($el['id']) && ($el['id'] === $std->id)) {
                        $found = true;
                        break;
                    }
                }
                if ($i >= 0 && $found) {
                    $el = $list[$i];
                    $std->nom = strtoupper($el['nom']);
                    $std->prenom = ucfirst($el['prenom']);
                    $std->email = strtolower($el['email']);
                    $std->gsm = $el['gsm'];
                    $std->save();
                    $addition++;
                    unset($list[$i]);
                } else {
                    array_push($delete_list, $std->id);
                }
            }
            //that means there still more elements in our array
            //if the element doesn't have an ID we will delete it other ways we will create it
            if (count($list) > 0) {
                foreach ($list as $el) {
                    if (isset($el['id'])) {
                        Etudiant::destroy($el['id']);
                    } else {
                        Etudiant::create([
                            "nom" => $el['nom'],
                            "prenom" => $el['prenom'],
                            "email" => $el['email'],
                            "gsm" => $el['gsm'],
                            "groupe_id" => $user->id
                        ]);
                    }
                }
            }
            //Delete users list
            if (count($delete_list) > 0) {
                Etudiant::destroy($delete_list);
            }
            return $this->formIndex($request);
        } catch (\Exception $ex) {
            return $ex;
            return response()->json([
                "message" => "Quelque chose a mal tourné"
            ], 422);
        }
    }
}
