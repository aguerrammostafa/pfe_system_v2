<?php

namespace App\Http\Controllers;

use App\Etudiant;
use App\Events\GroupCreated;
use App\Groupe;
use App\Jobs\GroupCreatedJob;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Resources\DateLimitTypes;
class SignupController extends Controller
{

    public function __construct()
    {
        $this->middleware(["limit_date:".DateLimitTypes::$DATE_LIMIT_INSCREPTION])->only(["store"]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "username" => ['required', 'string', 'max:255', "min:4", 'unique:groupes', 'regex:'.config("app.username_regex")],
            "password" => "min:8|max:255|required_with:r_password|same:r_password",
            "r_password" => "required",
            "filiere" => "required|in:GTR,GI",
            "list" => "required|array|min:1|max:" . config("app.max_students"),
            "list.*.nom" => "required|max:255|min:2|regex:/^[a-zA-Z\s\-\']*$/",
            "list.*.prenom" => "required|max:255|min:2|regex:/^[a-zA-Z\s\-\']*$/",
            "list.*.email" => "required|email|unique:groupes",
            "list.*.gsm" => ["required", "regex:" . config("app.phone_regex")],
        ],
            [
                "username.required" => "Le nom d'utilisateur est requis.",
                "username.unique" => "Le nom d'utilisateur existe déjà.",
                "username.min" => "Le nom d'utilisateur doit contenir au moins 4 caractères.",
                "password.required" => "Le mot de passe est requis",
                "password.min" => "Le mot de passe doit contenir au moins 8 caractères.",
                "r_password.required" => "La confirmation du mot de passe est requise.",
                "password.same" => "Les mots de passes ne correspondent pas."

            ]);

        $list = $request->list;
        $grp = Groupe::create([
            "email" => strtolower($list[0]["email"]),
            "filiere" => $request->filiere,
            "password" => $request->password,
            "username" => strtolower($request->username),
            "remember_token"=>Str::random(32),
            "encadrant_id"=>0
        ]);

        foreach ($list as $et) {
            Etudiant::create([
                "nom" => strtoupper($et["nom"]),
                "prenom" => ucfirst($et["prenom"]),
                "email" => strtolower($et["email"]),
                "gsm" => strtolower($et["gsm"]),
                "groupe_id" => $grp->id,

            ]);
        }
        dispatch(new GroupCreatedJob($grp));
//        event(new GroupCreated($grp));
        return [
            "message" => "Votre compte a été créé avec succès, veuillez vérifier votre email [" . $grp->email . "]"
        ];
    }

    public function activateAccount(Request $request, $token)
    {
        $grp = Groupe::where("remember_token",$token)->first();
        if($grp)
        {
            $grp->activate();
            return view("activated");
        }

        return redirect("/");
    }

}
