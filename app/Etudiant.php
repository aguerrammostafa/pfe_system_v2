<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etudiant extends Model
{
    protected $guarded = ["id"];
    public function groupe()
    {
        return $this->belongsTo('App\Groupe', 'groupe_id', 'id');
    }
    public function setNomAttribute($value){
        $this->attributes['nom'] = ucfirst($value);
    }
    public function setPrenomAttribute($value){
        $this->attributes['prenom'] = ucfirst($value);
    }
}
