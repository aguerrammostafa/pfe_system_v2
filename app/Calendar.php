<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $guarded = ["id"];


    public function setAtDateAttribute($value)
    {
        try{
            $date = new DateTime($value);
            $this->attributes['at_date'] = $date->format("Y-m-d");
        }
        catch (\Exception $ex)
        {
            log($ex);
        }
    }
}
