<?php

namespace App\Listeners;

use App\Events\GroupCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendVerificationCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GroupCreated  $event
     * @return void
     */
    public function handle(GroupCreated $event)
    {
        $group = $event->group;
        if($group->getRememberToken() != null)
        {
            Mail::to($group->email)->send(new \App\Mail\GroupCreated($group->getRememberToken()));
        }
    }
}
