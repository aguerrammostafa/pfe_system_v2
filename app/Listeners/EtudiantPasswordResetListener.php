<?php

namespace App\Listeners;

use App\Events\EtudiantPasswordResetEvent;
use App\Mail\PasswordResetEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EtudiantPasswordResetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EtudiantPasswordResetEvent  $event
     * @return void
     */
    public function handle(EtudiantPasswordResetEvent $event)
    {
        $passwordReset = $event->passwordReset;
        Mail::to($passwordReset->email)->send(
            new PasswordResetEmail($passwordReset->token,$passwordReset->email)
        );
    }
}
