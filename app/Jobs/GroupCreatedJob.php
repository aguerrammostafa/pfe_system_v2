<?php

namespace App\Jobs;

use App\Events\GroupCreated;
use App\Groupe;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GroupCreatedJob implements ShouldQueue
{
    public $group;
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @param Groupe $group
     */
    public function __construct(Groupe $group)
    {
        $this->group = $group;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        event(new GroupCreated($this->group));
    }
}
