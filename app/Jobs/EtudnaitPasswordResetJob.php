<?php

namespace App\Jobs;

use App\Events\EtudiantPasswordResetEvent;
use App\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class EtudnaitPasswordResetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var PasswordReset
     */
    public $passwordReset;
    public $tries = 3;


    /**
     * Create a new job instance.
     *
     * @param PasswordReset $pr
     */
    public function __construct(PasswordReset $pr)
    {
        $this->passwordReset = $pr;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        event(new EtudiantPasswordResetEvent($this->passwordReset));
    }


}
