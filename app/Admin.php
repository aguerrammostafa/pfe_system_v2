<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use App\Encadrant;
class Admin extends Authenticatable implements JWTSubject
{
    protected $guarded = ["id"];
    use Notifiable;

    protected $hidden = [
        "password"
    ];

    public function encadrant()
    {
        return Encadrant::where("id",$this->attributes['encadrant_id'])->first();
    }

    public function comparePassword($password)
    {
        return Hash::check($password,$this->attributes['password']);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
        $this->attributes['clear_password'] = $value;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            "admin" => $this->attributes["encadrant_id"] == null
        ];
    }
}
