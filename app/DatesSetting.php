<?php

namespace App;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class DatesSetting extends Model
{
    protected $table = "dates_setting";
    protected $fillable = ["title","at"];
    public function setAtAttribute($value)
    {
        try{
            $date = new DateTime($value);
            $this->attributes['at'] = $date->format("Y-m-d");
        }
        catch (\Exception $ex)
        {
            log($ex);
        }
    }
}
