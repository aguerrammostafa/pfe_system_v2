<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jury extends Model
{
    protected $table = "juries";
    protected $fillable = ["title"];

    public function encadrants($id = null)
    {
        if ($id != null)
            return $this->hasMany(\App\Encadrant::class, "jury")->where("id", "!=", $id);
        return $this->hasMany(\App\Encadrant::class, "jury");
    }
    public static function nonEnc()
    {
        return \App\Encadrant::where("jury", 0);
    }

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($model) {
            foreach ($model->encadrants as $el) {
                $el->jury = 0;
                $el->save();
            }
        });
    }
}
