<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Report extends Model
{
    protected $fillable = ["link","title","groupe_id","active","find","isFinal"];
    public function groupe(){
        return $this->belongsTo(\App\Groupe::class);
    }
    public static function boot()
    {
        parent::boot();
        static::deleting(function($model){
            Storage::delete($model->link);
        });
    }
}
