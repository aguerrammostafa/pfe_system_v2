import React, {useEffect,useState} from 'react';
import {ProgressBar} from "@blueprintjs/core";
import {ButtonRedux} from "../../data/connect";
/**
 * @param check
 * @param loading_id
 * @param className
 * @param props
 * */
const LazyLoader = ({check,loading_id,className,style, ...props}) => {
    const [render, setRender] = useState(false);
    useEffect(()=>{
        if(loading_id)
        {
            if(!props.loading[loading_id])
            {
                setRender(true)
            }
            else{
                setRender(false)
            }
        }
        else {
            if(check)
            {
                setRender(true)
            }
            else{
                setRender(false)
            }
        }
    },[check,props.loading]);
    if (!render) {
        return <div style={style} className={`${className}`}>
            <ProgressBar intent={"primary"}/>
        </div>
    } else
        return (
            {...props.children}
        );
}

export default ButtonRedux(LazyLoader)
