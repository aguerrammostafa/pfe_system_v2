import React, {useEffect} from 'react';
import Header from "../shared/Header";

const Container = ({children,className,classNameContainer,...props}) => {

    return (
        <div {...props} className={`container ${className || ""}`}>
            <Header/>
            <div className={`row justify-content-center pt-3 pb-5 ${classNameContainer || ""}`}>
                {children}
            </div>
        </div>
    );
}

export default Container
