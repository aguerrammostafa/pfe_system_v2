import React, {useEffect, useState} from 'react';
import Calendar from "../../shared/Calendar";
import {Callout, Card, Tab, Tabs, Divider} from "@blueprintjs/core";
import Button from "../../forms/Button";
import {DashboardRedux} from "../../../data/connect";
import Container from "../../layouts/Container";
import BreadcrumbCostume from "../../forms/Breadcrumb";
import ListDesPfe from "./ListDesPFE";
import EditForm from "./EditForm";
import Report from "./Report";
import UsefulDocuments from "./UsefulDocuments";
import MyPdf from "./MyPDF";
import PfeTerms from "./PFETerms";
import Alert from '../../forms/Alert';
import HomePanel from "./HomePanel"
import { CompareDates } from '../../../utils/CompareDates';
// import Encadrants from "./Encadrants";

const Index = (props) => {
    const [tab, setTab] = useState("home")
    const [messageShow, setMessageShow] = useState(true)
    useEffect(()=>{
        if(messageShow && props.reports_list.length===0)
        {
            let daysLeft = CompareDates(props.settings.date_ins_rap)
            if(daysLeft>=0 && daysLeft < 15 && props.reports_list.length<1)
            {
                props.message(`Il vous reste ${daysLeft} jours pour télécharger votre rapport`,"check_reports","warning")
                setMessageShow(false)
            }
        }

    });
    const handleTabChange = (navId) => {
        setTab(navId)
    }
    return (
        <Container>
            <Card className="w-100">
                <Alert message_id="check_reports" />
                <div className="row pb-3">
                    <BreadcrumbCostume items={[
                        {
                            href: "/", text: "Accueil", icon: "home"
                        },
                        {
                            text: "Tableau de bord", icon: "dashboard"
                        },
                    ]}/>
                </div>
                <div className="row">
                    <Tabs className={"w-100 dashboard-tabs"} vertical id="TabsExample" onChange={handleTabChange}
                          selectedTabId={tab}>
                        <Callout>
                        <span className="d-flex justify-content-center">
                            <span style={{color: "#878080"}}>Menu</span>
                        </span>
                        </Callout>
                        <Tab id="home" title="Accueil" panel={<HomePanel/>}/>
                        <Tab id="tab1" title="Calendrier" panel={<Calendar/>}/>
                         <Tab id="tab2" title="Modalités des PFE" panel={<PfeTerms/>}/>
                        <Tab className="w-100" id="tab3" title="Liste des PFE" panel={<ListDesPfe/>}/>
                        <Tab id="tab4" title="Ma fiche PFE" panel={<MyPdf/>}/>
                        <Tab id="tab5" title="Formulaire PFE" panel={<EditForm/>}/>
                        <Tab id="tab6" title="Documents Utiles" panel={<UsefulDocuments/>}/>
                        {/* <Tab id="tab7" title="Encadrants" panel={null}/> */}
                        <Tab id="tab8" title="Rapport PFE" panel={<Report/>}/>
                        <Button minimal onClick={() => props.logout()} className="w-100" intent="danger"
                                icon={"log-out"}>Déconnexion</Button>
                    </Tabs>
                </div>
            </Card>
        </Container>
    );
}

export default DashboardRedux(Index)
