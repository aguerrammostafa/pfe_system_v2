import React, {useEffect,useState} from 'react'
import LazyLoader from '../../layouts/LazyLoader'
import {Alert, HTMLTable} from '@blueprintjs/core'
import {UserHomePanel} from "../../../data/connect";
import Button from "../../forms/Button";
import axios from "../../../utils/axios"
const HomePanel = (props)=>{
    const [encadrant,setEncadrant] = useState(null);
    const [isOpen,setIsOpen] = useState(false)
    const [note,setNote] = useState(null)
    useEffect(()=>{
        props.init(initEnc)

        axios.get("/me/note/")
            .then(({data})=>{
                setNote(data)
            })
            .catch((ex)=>{

            })
    },[])
    useEffect(()=>{

    },[encadrant])
    const initEnc = (payload)=>{
        setEncadrant(payload)
        console.log(payload)
    }
    const openAlert  = (open=true)=>{
        setIsOpen(open)
    }
    const closeAlert  = (open=false)=>{
        setIsOpen(open)
    }
    const confirmDelete  = ()=>{
        setIsOpen(false)
        props.deleteAccount(()=>{
            props.logout()
        })
    }
    return(
        <div>
            <LazyLoader loading_id={"enc_loading"} className="w-100 p-2">
                <div className="w-100">
                    <h2>Votre encadrant : </h2>
                    {encadrant?<HTMLTable className="w-100">
                        <thead>
                        <tr>
                            <td>Nom</td>
                            <td>Prénom</td>
                            <td>Email</td>
                            <td>GSM</td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{encadrant.nom}</td>
                            <td>{encadrant.prenom}</td>
                            <td>{encadrant.email}</td>
                            <td>{encadrant.gsm}</td>
                        </tr>
                        </tbody>
                    </HTMLTable>:<p>
                        Vous n'avez pas encore d'encadrant.

                    </p>}
                    <h2>Ma note : </h2>
                    {
                        note ?
                            <div className={"p-2"} style={{border:"1px solid #d2c9c9"}}>
                                <h4>{note.note}</h4>
                                <p>{note.remark}</p>
                            </div>
                            :
                            <p>Il n'y a pas encore de note</p>
                    }
                </div>
            </LazyLoader>
            <div className="w-100">
                <h2>Actions sur votre compte</h2>
                <div>
                    <Button loading_id={"me_delete"} onClick={openAlert} icon={"trash"} intent={"danger"}>Supprimer votre compte</Button>
                </div>

                <Alert
                    confirmButtonText={"Oui je veux le supprimer"}
                    isOpen={isOpen}
                    intent={"danger"}
                    icon={"trash"}
                    cancelButtonText={"Annuler"}
                    onClose={closeAlert}
                    canOutsideClickCancel={closeAlert}
                    canEscapeKeyCancel={closeAlert}
                    onConfirm={confirmDelete}
                >
                    <p>Si vous supprimez votre compte, toutes vos données disparaîtront.</p>
                </Alert>
            </div>
        </div>
    )
}
export default UserHomePanel(HomePanel)
