import React, {useEffect, useState} from 'react';
import {Tab, Tabs} from "@blueprintjs/core";
import FiliereList from "../../shared/FiliereList"
import {Filieres} from "../../../config/config";
const ListDesPfe = (props) => {
    const [tab, setTab] = useState("gi_tab")
    const handleTabChange = (navId) => {
        setTab(navId)
    };
    return (
        <Tabs animate={false} className="w-100" selectedTabId={tab} onChange={handleTabChange}>
            <Tab id={"gi_tab"} title={"Liste PFE G-Info"} panel={<FiliereList filiere={Filieres.gi}/>}/>
            <Tab id={"gtr_tab"} title={"Liste PFE GTR"} panel={<FiliereList filiere={Filieres.gtr}/>}/>
        </Tabs>
    );
}

export default ListDesPfe
