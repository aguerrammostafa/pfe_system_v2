import React, {useEffect, useState} from 'react';
import {FileInput, FormGroup, HTMLTable, Checkbox, Switch} from "@blueprintjs/core";
import Button from "../../forms/Button";
import {ReportRedux} from "../../../data/connect";
import Alert from "../../forms/Alert";
import config from "../../../config/config";
import LazyLoader from "../../layouts/LazyLoader";

const Report = (props) => {
    const [file, setFile] = useState(null)
    const [isFinal,setIsFinal] = useState(false);
    const [canDelete, setCanDelete] = useState(false);
    useEffect(() => {
        props.initList()
    }, []);
    const inputHandler = (v) => {
        let files = v.target.files
        let errors = []
        let _file
        if (files.length > 0) {
            _file = files[0]
            let s = _file.name.split(".") //to store spited variable
            if (s.length > 0) {
                let ext = s[s.length - 1]
                if (_file.size > config.MAXIMUM_FILE_SIZE) {
                    errors.push(`La taille de fichier maximale autorisée est ${config.MAXIMUM_FILE_SIZE / 1000000}mb`)
                }
                if (ext.toUpperCase() !== "PDF") {
                    errors.push("Extension de fichier non autorisée, seulement PDF")
                }
            } else {
                errors.push("Extension de fichier non autorisée, seulement PDF")
            }
        }

        if (errors.length > 0) {
            props.setErrorMessage(errors.join("<br/>"))
        } else {
            //store file in state
            setFile(_file)
        }
    }
    const deleteReport = (id)=>{
        props.deleteReport(id)

    }
    const stratUploading = () => {
        file && props.uploadReport(file,isFinal==true?1:0,setFile);
    }
    const finalChange = ()=>{
        setIsFinal(!isFinal)
    }
    return (
        <div>
            <div className="row justify-content-center flex-column align-items-center">
                <Alert message_id={"report_upload"}/>
                <FormGroup
                    label={"Importer un rapport de PFE"}
                    labelInfo={"(PDF)"}
                >
                    <FileInput
                        id={"repport-upload"}
                        buttonText={"importer"}
                        text={file ? file.name : "Rapport..."}
                        onInputChange={inputHandler}
                        accept={"application/pdf"}
                    />
                </FormGroup>
                <Switch checked={isFinal} label="Rapport final" onChange={finalChange}>

                </Switch>
                <div className="row">
                    <Button loading_id={"upload_report"} onClick={stratUploading} disabled={!file} intent={"success"}
                            icon={"upload"} text={"Importer"}/>
                </div>
                <div className="row col-12">
                    <Alert message_id={"upload_report_message"}/>
                </div>
            </div>
            <LazyLoader loading_id={"get_reports_list"} className="row pr-3 mt-2 ">
                <HTMLTable className="w-100">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Lien</th>
                        <th>Téléchargé à</th>
                        <th>Type du rapport</th>
                        <th className="w-25">Action <a onClick={()=>{
                            setCanDelete(!canDelete)
                        }}><small>{canDelete?"(désactiver la suppression)":"(activer la suppression)"}</small></a></th>
                    </tr>
                    </thead>
                    <tbody>
                    {props.reports_list && props.reports_list.map((e,i) => {
                        return <tr key={e.id}>
                            <td>{i+1}</td>
                            <td><a href={`/api/report/download/${e.find}`} title={e.title} download={true}>{e.title}</a></td>
                            <td>{e.created_at}</td>
                            <td>{e.isFinal==1?"Finale":"Avance"}</td>
                            <td><Button disabled={!canDelete} loading_id={`delete_report_${e.id}`} onClick={()=>deleteReport(e.id)} text={"Supprimer"} minimal intent={"danger"} icon={"trash"}/></td>
                        </tr>
                    })}

                    </tbody>
                </HTMLTable>
            </LazyLoader>
            <Alert message_id={"report_delete"}/>
        </div>
    );

}

export default ReportRedux(Report)
