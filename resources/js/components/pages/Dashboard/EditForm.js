import React, { useEffect, useState } from "react";
import SignupForm from "../Signup/SignupForm";
import { EditFormRedux } from "../../../data/connect";
import Button from "../../forms/Button";
import {
    Divider,
    FormGroup,
    InputGroup,
    TextArea,
    Callout
} from "@blueprintjs/core";
import Alert from "../../forms/Alert";
import DatePickerCostume from "../../forms/DatePicker";
import LazyLoader from "../../layouts/LazyLoader";
import CostumeSelect from "../../forms/Select";
import { PFETypes } from "../../../config/config";
import { CompareDates } from "../../../utils/CompareDates";

const EditForm = props => {
    const [datePassed, setDatePassed] = useState(false);
    useEffect(() => {
        if (!props.editform.version) {
            props.initData();
        }
        setDatePassed(CompareDates(props.settings.date_com_don) < 0);
    }, [props.editform && props.editform.version]);
    const submitForm = e => {
        e.preventDefault();
        props.update_form_data(props.editform);
    };

    return (
        <LazyLoader
            className={"row justify-content-center pr-3"}
            check={props.editform && props.editform.version}
        >
            <form onSubmit={submitForm} className="w-100 ">
                {datePassed && (
                    <div className="row pr-3">
                        <Callout intent="danger">
                            La date de modification des données est passée
                        </Callout>
                    </div>
                )}
                <SignupForm
                    datePassed={datePassed}
                    data={props.editform}
                    {...props}
                />

                <p>
                    <b>Thème du PFE</b>
                </p>
                <div className="row">
                    <div className="col-12 form-edit justify-content-center">
                        <div className="col-12 pr-3">
                            <FormGroup label="Intitulé du PFE :">
                                <InputGroup
                                    tabIndex={"20"}
                                    placeholder="Intitulé du PFE"
                                    type={"text"}
                                    value={
                                        props.editform.sujet
                                            ? props.editform.sujet
                                            : ""
                                    }
                                    onChange={el =>
                                        props.updateProps(
                                            "sujet",
                                            el.target.value
                                        )
                                    }
                                    disabled={datePassed}
                                />
                            </FormGroup>
                            <FormGroup label={"Déscription du PFE :"}>
                                <TextArea
                                    className={"costume-textarea"}
                                    tabIndex={"21"}
                                    fill
                                    rows={5}
                                    placeholder="Déscription du PFE"
                                    value={
                                        props.editform.desc
                                            ? props.editform.desc
                                            : ""
                                    }
                                    onChange={el =>
                                        props.updateProps(
                                            "desc",
                                            el.target.value
                                        )
                                    }
                                    disabled={datePassed}
                                />
                            </FormGroup>
                            <FormGroup
                                label="Mots-clés:"
                                labelInfo={"Séparés par une virgule"}
                            >
                                <InputGroup
                                    tabIndex={"22"}
                                    placeholder="Mots-clés"
                                    type={"text"}
                                    value={
                                        props.editform.mots_cle
                                            ? props.editform.mots_cle
                                            : ""
                                    }
                                    onChange={el =>
                                        props.updateProps(
                                            "mots_cle",
                                            el.target.value
                                        )
                                    }
                                    disabled={datePassed}
                                />
                            </FormGroup>
                            <div className="col-12 col-md-8 col-lg-6">
                                <FormGroup
                                    label="Durée de stage:"
                                    labelInfo="(Jours)"
                                    inline
                                >
                                    <InputGroup
                                        tabIndex={"23"}
                                        placeholder="Durée de stage"
                                        type={"number"}
                                        value={props.editform.duree_stage}
                                        onChange={el =>
                                            props.updateProps(
                                                "duree_stage",
                                                el.target.value
                                            )
                                        }
                                        disabled={datePassed}
                                    />
                                </FormGroup>
                                <FormGroup label="Date début de stage :" inline>
                                    <DatePickerCostume
                                        tabIndex={"24"}
                                        value={props.editform.debut_stage}
                                        onChange={val => {
                                            props.updateProps(
                                                "debut_stage",
                                                val
                                            );
                                        }}
                                        disabled={datePassed}
                                    />
                                </FormGroup>
                                <FormGroup inline label={"PFE pré-embauche :"}>
                                    <CostumeSelect
                                        className={"col-12"}
                                        tabIndex={"25"}
                                        options={PFETypes}
                                        defaultValue={PFETypes.find(
                                            e =>
                                                e.value ===
                                                    props.editform
                                                        .pre_embauche || 0
                                        )}
                                        onChange={val => {
                                            props.updateProps(
                                                "pre_embauche",
                                                val.value
                                            );
                                        }}
                                        disabled={datePassed}
                                    />
                                </FormGroup>
                            </div>
                        </div>
                    </div>
                </div>
                <p>
                    <b>Entreprise & Encadrement externe</b>
                </p>
                <div className="row w-100">
                    <Divider className="w-100" />
                </div>
                <div className="row col-12">
                    <div className="col-12 pr-3">
                        <FormGroup label={"Entreprise & Département :"}>
                            <TextArea
                                className={"costume-textarea"}
                                tabIndex={"26"}
                                fill
                                rows={5}
                                placeholder="Entreprise & Département"
                                value={
                                    props.editform.entreprise
                                        ? props.editform.entreprise
                                        : ""
                                }
                                onChange={el =>
                                    props.updateProps(
                                        "entreprise",
                                        el.target.value
                                    )
                                }
                                disabled={datePassed}
                            />
                        </FormGroup>
                        <FormGroup label={"Coordonnés et Web :"}>
                            <TextArea
                                className={"costume-textarea"}
                                tabIndex={"27"}
                                fill
                                rows={5}
                                placeholder="Coordonnés et Web"
                                value={
                                    props.editform.coordonnes_entreprise
                                        ? props.editform.coordonnes_entreprise
                                        : ""
                                }
                                onChange={el =>
                                    props.updateProps(
                                        "coordonnes_entreprise",
                                        el.target.value
                                    )
                                }
                                disabled={datePassed}
                            />
                        </FormGroup>
                        <FormGroup label={"Ville :"} inline>
                            <InputGroup
                                tabIndex={"28"}
                                placeholder="Ville"
                                type={"text"}
                                value={props.editform.ville_entreprise}
                                onChange={el =>
                                    props.updateProps(
                                        "ville_entreprise",
                                        el.target.value
                                    )
                                }
                                disabled={datePassed}
                            />
                        </FormGroup>
                        <FormGroup
                            label={"Nom/prénom de l'encadrant externe :"}
                            inline
                        >
                            <InputGroup
                                tabIndex={"29"}
                                placeholder="Nom/prénom de l'encadrant externe"
                                type={"text"}
                                value={props.editform.nom_encadrant_ext}
                                onChange={el =>
                                    props.updateProps(
                                        "nom_encadrant_ext",
                                        el.target.value
                                    )
                                }
                                disabled={datePassed}
                            />
                        </FormGroup>
                        <FormGroup
                            label={
                                "Fonction de l'encadrant au sein de l'entreprise :"
                            }
                            inline
                        >
                            <InputGroup
                                tabIndex={"30"}
                                placeholder="Fonction de l'encadrant au sein de l'entreprise"
                                type={"text"}
                                value={props.editform.fonction_encadrant_ext}
                                onChange={el =>
                                    props.updateProps(
                                        "fonction_encadrant_ext",
                                        el.target.value
                                    )
                                }
                                disabled={datePassed}
                            />
                        </FormGroup>
                        <FormGroup label={"Téléphone / GSM :"} inline>
                            <InputGroup
                                tabIndex={"32"}
                                placeholder="Téléphone / GSM"
                                type={"text"}
                                value={props.editform.gsm_encadrant_ext}
                                onChange={el =>
                                    props.updateProps(
                                        "gsm_encadrant_ext",
                                        el.target.value
                                    )
                                }
                                disabled={datePassed}
                            />
                        </FormGroup>
                        <FormGroup label={"Email :"} inline>
                            <InputGroup
                                tabIndex={"32"}
                                placeholder="Email"
                                type={"email"}
                                value={props.editform.email_encadrant_ext || ""}
                                onChange={el =>
                                    props.updateProps(
                                        "email_encadrant_ext",
                                        el.target.value
                                    )
                                }
                                disabled={datePassed}
                            />
                        </FormGroup>
                    </div>
                </div>
                <div className="row w-100">
                    <Divider className="w-100" />
                    <Alert message_id={"editform_message"} />
                    <Alert message_id={"editform_message_toast"} />
                </div>
                <div className="row justify-content-end">
                    <Button
                        tabIndex={"99"}
                        disabled={props.editform.version <= 1}
                        type={"submit"}
                        loading_id={"editform_loading"}
                        icon={"tick-circle"}
                        intent={"success"}
                        text={"Enregistrer"}
                    />
                </div>
            </form>
        </LazyLoader>
    );
};

export default EditFormRedux(EditForm);
