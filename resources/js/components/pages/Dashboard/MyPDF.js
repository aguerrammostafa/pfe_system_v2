import React, {useEffect, useState,useRef} from 'react';
import {Icon} from "@blueprintjs/core";
import {MyPdfRedux} from "../../../data/connect";
import Button from "../../forms/Button";
import LazyLoader from "../../layouts/LazyLoader";

const MyPdf = (props) => {
    const frameRef = useRef(null);
    useEffect(() => {
        props.fetch()
    }, []);

    return (
        <div className="w-100 pr-3 h-100 pb-5">
            <Button
                loading_id={"pdf_pfe_download"}
                onClick={() => {
                    props.download()
                }} intent={"primary"} fill><Icon icon={"download"}/> &nbsp;&nbsp;&nbsp; Telecharger ma fiche
                PFE</Button>

            <LazyLoader loading_id={"fetch_my_document"} className="pr-md-2 min-h-100">
                <div className="w-100 h-100">
                    <iframe className="w-100 h-100" src={`data:text/html,${encodeURIComponent(props.my_document.html)}`}/>
                </div>
            </LazyLoader>

        </div>
    );
}

export default MyPdfRedux(MyPdf)
