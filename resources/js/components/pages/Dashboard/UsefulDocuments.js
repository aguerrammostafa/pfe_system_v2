import React from 'react';
import Button from "../../forms/Button";
import Alert from "../../forms/Alert";
import {FormGroup, HTMLTable, Divider} from "@blueprintjs/core";
import LazyLoader from "../../layouts/LazyLoader";

const UsefulDocuments = () => {
    return (
        <div className={"w-100"}>
            <h4>Document utiles</h4>
            <Divider/>
            <div>
                <p>Télécharger ici les documents necessaires à votre PFE :</p>
                <br/>
            </div>
            <div className="pl-md-3">
                <p><a href="/download/convention.pdf">Convention de stage </a> (à telecharger et faire signer par l'entreprise et l'ENSAS)</p>
                <p><a href="/download/evaluation.pdf">Fiche d'évaluation industrielle </a> (à télécharger et à remettre à l'encadrant industriel)</p>
                <p><a href="/download/page_de_garde_pfe.doc">Modèle de page de garde </a> </p>
                <p><a href="/download/page_finale_pfe.doc">Modèle de page finale </a> </p>
                <p><a href="/download/references.doc">Modèle de référence bibliographiqu </a> </p>
                <p><a href="/download/mask.pdf">Modèle du masque PowerPoint </a> </p>
            </div>
        </div>
    )
}
export default UsefulDocuments;
