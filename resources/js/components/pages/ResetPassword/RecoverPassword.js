import React, {useEffect, useState,useRef} from 'react';
import Container from "../../layouts/Container";
import {Card, FormGroup, InputGroup, Intent, ProgressBar, Tooltip} from "@blueprintjs/core";
import BreadcrumbCostume from "../../forms/Breadcrumb";
import NotFound from "../NotFound";
import {RecoverPasswordRedux} from "../../../data/connect";
import Button from "../../forms/Button";
import Alert from "../../forms/Alert";

const RecoverPassword = (props) => {
    const [valid, setValid] = useState(true);
    const [canAccess, setCanAccess] = useState(false);
    const [showPassword,setShowPassword] = useState(false)

    const [token,setToken] = useState(null);
    const [email,setEmail] = useState(null);

    const passwordRef = useRef("");
    const rPasswordRef = useRef("");


    const lockButton = (
        <Tooltip content={`${showPassword ? "Cacher" : "Montrer"} le mot de passe`}>
            <Button
                icon={showPassword ? "eye-open" : "eye-off"}
                intent={Intent.WARNING}
                minimal={true}
                onClick={()=>setShowPassword(!showPassword)}
            />
        </Tooltip>
    );
    const tokenNotValid = () => {
        setValid(false)
    }
    const tokenValid = () => {
        setCanAccess(true)
    }

    const onSuccess = ()=>{
        passwordRef.current.value = "";
        rPasswordRef.current.value = "";
    }
    const submitForm = (e)=>{
        e.preventDefault();
        props.updatePassword(
            token,
            email,
            passwordRef.current.value,
            rPasswordRef.current.value,
            onSuccess
        )
    };

    useEffect(() => {
        let {token:t} = props.match.params;
        let {email:e} = props.match.params;
        props.checkToken(t, e, tokenValid, tokenNotValid)

        setEmail(e)
        setToken(t)

    }, []);
    if (valid) {
        return <Container>
            <Card className="w-100">
                <BreadcrumbCostume items={[
                    {
                        href: "/", text: "Accueil", icon: "home"
                    },
                    {
                        text: "Réinitialiser le mot de passe", icon: "key"
                    },
                ]}/>
                {
                    canAccess ?
                        <div className="row justify-content-center">
                            <Card className="w-100 mt-3">
                                <h4>Choisissez un autre mot de passe</h4>
                                <div className="row justify-content-center">
                                    <form onSubmit={submitForm} className="col-12 col-md-6">
                                        <FormGroup
                                            label={"Password :"}
                                        >
                                            <InputGroup
                                                tabIndex={1}
                                                leftIcon={"lock"}
                                                placeholder="Enter your password..."
                                                rightElement={lockButton}
                                                type={showPassword ? "text" : "password"}
                                                inputRef={passwordRef}
                                            />
                                        </FormGroup>
                                        <FormGroup
                                            label={"Répéter votre mot de passe\n"}
                                        >
                                            <InputGroup
                                                tabIndex={2}
                                                leftIcon={"lock"}
                                                placeholder="Enter your password..."
                                                type={"password"}
                                                inputRef={rPasswordRef}
                                            />
                                        </FormGroup>
                                        <Button type={"submit"} loading_id={"reset_password"} tabIndex={3} intent={Intent.SUCCESS} icon={"floppy-disk"} text={"Enregistrer"}/>
                                        <Alert message_id={"reset_password_m"}/>
                                    </form>
                                </div>
                            </Card>
                        </div> : <div className="row">
                            <ProgressBar intent={Intent.PRIMARY}/>
                        </div>
                }
            </Card>
        </Container>
    } else {
        return <NotFound/>
    }
}

export default RecoverPasswordRedux(RecoverPassword)
