import React, {useEffect,useState,useRef} from 'react'
import Container from '../../layouts/Container'
import {Card, Divider, InputGroup} from "@blueprintjs/core";
import Button from "../../forms/Button";
import {Intent} from "@blueprintjs/core/lib/cjs";
import BreadcrumbCostume from "../../forms/Breadcrumb";
import Alert from "../../forms/Alert";
import {ResetPasswordRedux} from "../../../data/connect";

const Index = (props)=>{
    const usernameRef = useRef("")

    const submitForm = (e)=>{
        e.preventDefault()
        props.resetPassword(usernameRef.current.value,resetUserField)
    }
    const resetUserField = ()=>{
        usernameRef.current.value = "";
    }
    return(
        <Container>

            <Card className="w-100">
                <BreadcrumbCostume items={[
                    {
                        href: "/", text: "Accueil", icon: "home"
                    },
                    {
                        text: "Réinitialiser le mot de passe", icon: "key"
                    },
                ]}/>
                <Divider/>
                <div className="row justify-content-center">
                    <form className="col-12 col-md-4" onSubmit={submitForm}>
                        <InputGroup
                            leftIcon={"user"}
                            placeholder={"Email"}
                            inputRef={usernameRef}
                            type={"text"}
                            name={"email"}
                        />
                        <br/>
                        <div className="d-flex justify-content-end">
                            <Button
                                icon={"reset"}
                                intent={Intent.PRIMARY}
                                minimal={false}
                                onClick={()=>{}}
                                text={"Réinitialiser"}
                                type={"submit"}
                                loading_id={"reset_loading"}
                            />
                        </div>
                        <br/>
                        <Alert  message_id={"reset_password"} />
                    </form>
                </div>
            </Card>
        </Container>
    )
}
export default ResetPasswordRedux(Index)
