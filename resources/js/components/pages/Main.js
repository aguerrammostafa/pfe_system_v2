import React, { useState, useEffect } from "react";
import Login from "../shared/Login";
import Header from "../shared/Header";
import { Link, Redirect } from "react-router-dom";
import Button from "../forms/Button";
import { Callout, Card } from "@blueprintjs/core";
import Calendar from "../shared/Calendar";
import FiliereList from "../shared/FiliereList";
import { MainRedux } from "../../data/connect";
import Container from "../layouts/Container";
import { Filieres } from "../../config/config";

const Main = props => {
    const [page, setPage] = useState(0);
    let PAGE = <Calendar />;
    switch (page) {
        case 1:
            PAGE = <FiliereList filiere={Filieres.gi} />;
            break;
        case 2:
            PAGE = <FiliereList filiere={Filieres.gtr} />;
            break;
        case 3:
            PAGE = (
                <iframe
                    className="planning-frame"
                    src="/planning/planning.html"
                    frameBorder="0"
                    allowFullScreen
                    scrolling="auto"
                />
            );
            break;

        default:
            PAGE = <Calendar />;
    }
    const changePage = page => {
        setPage(page);
    };
    return (
        <Container classNameContainer="topHeader-Container">
            <div className="col-12 col-md-7 col-lg-9 pr-2">
                <TopSection pageChange={changePage} />
                <div className="row mt-2 topHeader-Container">
                    <Card className="w-100">{PAGE}</Card>
                </div>
            </div>
            <div className="col">
                <Login />
            </div>
        </Container>
    );
};
const TopSection = ({ pageChange }) => {
    return (
        <Card className="row justify-content-center">
            <Link to="/" className={"col-12 col-md-6 mt-sm-2 col-lg-auto"}>
                <Button
                    className="w-100"
                    onClick={() => pageChange(0)}
                    intent={"primary"}
                    icon={"calendar"}
                >
                    Calendrier
                </Button>
            </Link>
            <Link
                to="/"
                className={"col-12 col-md-6 mt-sm-2 col-lg-auto ml-lg-2"}
            >
                <Button
                    className="w-100"
                    onClick={() => pageChange(1)}
                    intent={"primary"}
                    icon={"th"}
                >
                    Liste des PFE G.INFO
                </Button>
            </Link>
            <Link
                to="/"
                className={"col-12 col-md-6 mt-sm-2 col-lg-auto ml-lg-2"}
            >
                <Button
                    className="w-100"
                    onClick={() => pageChange(2)}
                    intent={"primary"}
                    icon={"th"}
                >
                    Liste des PFE GTR
                </Button>
            </Link>
            <Link
                to="/"
                className={"col-12 col-md-6 mt-sm-2 col-lg-auto ml-lg-2"}
            >
                <Button
                    className="w-100"
                    onClick={() => pageChange(3)}
                    intent={"primary"}
                    icon={"th"}
                >
                    Planning des soutenances
                </Button>
            </Link>
        </Card>
    );
};
export default Main;
