import React from 'react';
import {Card, H1, Icon} from '@blueprintjs/core'
import {Link} from 'react-router-dom'
import Container from "../layouts/Container";
import BreadcrumbCostume from "../forms/Breadcrumb";

const NotFound = (props) => {
    return (
        <Container>
            <Card className="w-100">
                <BreadcrumbCostume items={[
                    {
                        href: "/", text: "Accueil", icon: "home"
                    },
                    {
                        text: "Erreur", icon: "error"
                    },
                ]}/>
            </Card>
            <Card className="col-12">
                <div className="d-flex justify-content-center h-100 align-items-center flex-column">
                    <H1>Page page non trouvée 404</H1>
                </div>
            </Card>
        </Container>
    );
}

export default NotFound
