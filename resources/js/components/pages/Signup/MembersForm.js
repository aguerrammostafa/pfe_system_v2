import React, {useEffect} from 'react';
import {Button, FormGroup, InputGroup} from "@blueprintjs/core";

/**
 * @param data
 * @param onInit
 * @param onChange
 * @param id
 * */
const MembersForm = ({data,disabled, onInit, onChange, id}) => {
    useEffect(() => {
        if (!data) {
            onInit(id)
        }
    }, [])
    const handleChange = (path, value) => {
        onChange(path, value, id)
    }
    return (
        <div className="row w-100 align-items-center">
            <div className="row">
                <Button minimal disabled style={{marginTop: "5px"}}>{id + 1}</Button>

            </div>
            <div className="row col">
                <FormGroup
                    label={"Nom"}
                    labelInfo={"*"}
                    className={"col-3 p-2"}
                >
                    <InputGroup
                        tabIndex={"5"}
                        type={"text"}
                        value={data.nom}
                        onChange={(val) => {
                            handleChange("nom", val.target.value)
                        }}
                        disabled={disabled}
                    />
                </FormGroup>
                <FormGroup
                    label={"Prénom"}
                    labelInfo={"*"}
                    className={"col-3 p-2"}


                >
                    <InputGroup
                        tabIndex={"6"}
                        type={"text"}
                        value={data.prenom}
                        onChange={(val) => {
                            handleChange("prenom", val.target.value)
                        }}
                        disabled={disabled}
                    />
                </FormGroup>
                <FormGroup
                    label={"Email"}
                    labelInfo={"*"}
                    className={"col-3 p-2"}

                >
                    <InputGroup
                        tabIndex={"7"}
                        type={"email"}
                        value={data.email}
                        onChange={(val) => {
                            handleChange("email", val.target.value)
                        }}
                        disabled={disabled}
                    />
                </FormGroup>
                <FormGroup
                    label={"GSM"}
                    labelInfo={"*"}
                    className={"col-3 p-2"}

                >
                    <InputGroup
                        tabIndex={"8"}
                        type={"text"}
                        value={data.gsm}
                        onChange={(val) => {
                            handleChange("gsm", val.target.value)
                        }}
                        disabled={disabled}
                    />
                </FormGroup>
            </div>
        </div>
    );
}

export default MembersForm
