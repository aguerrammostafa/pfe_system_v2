import React, {useEffect,useState} from 'react';
import Select from "../../forms/Select";
import config, {Filieres, FilieresTitle} from "../../../config/config";
import {Divider, InputGroup, Tooltip} from "@blueprintjs/core";
import Button from "../../forms/Button";
import MembersForm from "./MembersForm";
import {Intent} from "@blueprintjs/core/lib/cjs";

const SignupForm = ({data,datePassed,...props}) => {
    const [showPassword, setShowPassword] = useState(false)

    const lockButton = (
        <Tooltip content={`${showPassword ? "Cacher" : "Montrer"} le mot de passe`}>
            <Button
                icon={showPassword ? "eye-open" : "eye-off"}
                intent={Intent.WARNING}
                minimal={true}
                onClick={() => setShowPassword(!showPassword)}
            />
        </Tooltip>
    );
    const updateProps = (path, value, index = undefined) => {
        props.updateProps(path, value, index)
    }
    return (
        <>
            <p><b>Données d'accès</b></p>
            <div className="row">
                <table className="col-12 col-md-6 pr-md-2">
                    <tbody>
                    <tr>
                        <td>Filière :</td>
                        <td>
                            <Select
                                tabIndex={"1"}
                                options={[
                                    {value: "GI", label: FilieresTitle[Filieres.gi]},
                                    {value: "GTR", label: FilieresTitle[Filieres.gtr]},
                                ]}
                                defaultValue={{
                                    value: data.filiere,
                                    label: FilieresTitle[data.filiere]
                                }}
                                onChange={(val) => {
                                    updateProps("filiere", val.value)
                                }}
                                disabled={datePassed}
                            />
                        </td>

                    </tr>
                    <tr>
                        <td>Mot de passe :</td>
                        <td>
                            <InputGroup
                                tabIndex={"3"}
                                leftIcon={"lock"}
                                placeholder="Mot de passe"
                                rightElement={lockButton}
                                type={showPassword ? "text" : "password"}
                                value={data.password}
                                onChange={(el) => updateProps("password", el.target.value)}
                            />
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table className="col-12 col-md-6 pl-md-2">
                    <tbody>
                    <tr>
                        <td>Nom d'utilisateur :</td>
                        <td>
                            <InputGroup
                                tabIndex={"2"}
                                type={"text"}
                                value={data.username}
                                onChange={(el) => updateProps("username", el.target.value)}
                                disabled={datePassed}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Confirmation :</td>
                        <td>
                            <InputGroup
                                tabIndex={"4"}
                                leftIcon={"lock"}
                                placeholder="Confirmation"
                                rightElement={<Button icon={"eye-off"} disabled/>}
                                type={"password"}
                                value={data.r_password}
                                onChange={(el) => updateProps("r_password", el.target.value)}
                            />
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div className="row py-4">
                <Divider className="w-100"/>
            </div>
            <p><b>Membres du groupe PFE</b>&nbsp;&nbsp;<small>(L'e-mail du premier membre sera utilisé comme courrier principal)</small></p>
            <div className="row">
                {[...Array(data.total_members)].map((el, e) => {
                    return <MembersForm disabled={datePassed} onInit={props.createIndex} onChange={updateProps}
                                        data={data.list[e]} key={e} id={e}/>
                })}
                <div className="row w-100 justify-content-between">
                    <Button minimal disabled={datePassed || data.total_members <= 1}
                            onClick={() => props.createIndex(--data.total_members)} intent={"danger"}
                            icon={"minus"}>Supprimer membre</Button>
                    <Button
                        tabIndex={"9"}
                        minimal disabled={datePassed || data.total_members >= config.MAX_MEMBERS} onClick={() => {
                        props.createIndex(++data.total_members)
                        setTimeout(() => {
                            window.scrollTo(0, window.scrollY+64)
                        }, 50)
                    }} intent={"primary"} icon={"plus"}>Ajouter un nouveau membre</Button>
                </div>
            </div>
        </>
    );
}

export default SignupForm
