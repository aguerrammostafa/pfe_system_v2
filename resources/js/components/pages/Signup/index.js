import React, { useEffect, useState } from "react";
import Container from "../../layouts/Container";
import { Card, Divider, Callout } from "@blueprintjs/core";
import Button from "../../forms/Button";
import Breadcrumb from "../../forms/Breadcrumb";
import { SignupRedux } from "../../../data/connect";
import Alert from "../../forms/Alert";
import SignupForm from "./SignupForm";
import { CompareDates } from "../../../utils/CompareDates";

const breadcrumbs = [
    { href: "/", icon: "home", text: "Accueil" },
    { icon: "new-person", text: "Register" }
];

const Index = props => {
    useEffect(() => {
    }, [props.signup && props.signup.version, props.settings]);
    return (
        <Container>
            <Card className="w-100">
                <div className="row pb-3">
                    <Breadcrumb items={breadcrumbs} />
                </div>
                {CompareDates(props.settings.date_pre) >= 0? (
                    <form
                        onSubmit={e => {
                            e.preventDefault();
                            props.signup_form(props.signup);
                        }}
                        className="row flex-column"
                    >
                        <SignupForm data={props.signup} {...props} />
                        <div className="row w-100">
                            <Divider className="w-100" />
                            <Alert message_id={"signup_message"} />
                        </div>
                        <div className="row mt-2 justify-content-between">
                            <Button
                                minimal
                                disabled={props.signup.version == 0}
                                onClick={() => props.clearFields()}
                                intent={"danger"}
                                icon={"error"}
                            >
                                Champs clairs
                            </Button>
                            <Button
                                tabIndex={"10"}
                                type={"submit"}
                                loading_id={"signup_loading"}
                                icon={"tick-circle"}
                                intent={"success"}
                                text={"Enregistrer"}
                            />
                        </div>
                    </form>
                ) : (
                    <Callout intent="danger">
                        La date limite d'inscription est passée
                    </Callout>
                )}
            </Card>
        </Container>
    );
};

export default SignupRedux(Index);
