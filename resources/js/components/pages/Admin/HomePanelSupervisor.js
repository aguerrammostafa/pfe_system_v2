import React, { useEffect, useState } from "react";
import LazyLoader from "../../layouts/LazyLoader";
import { HomePanelSupervisorRedux } from "../../../data/connect";
import {
    HTMLTable,
    Icon,
    Overlay,
    Classes,
    Drawer,
    InputGroup
} from "@blueprintjs/core";
import Button from "../../forms/Button";
import { Intent } from "@blueprintjs/core/lib/cjs/common/intent";
import axios from "../../../utils/axios";

const HomePanelSupervisor = props => {
    const [list, setList] = useState([]);
    const [render, setRender] = useState(true);
    const [myStudents, setMyStudents] = useState([]);
    const [myTeamStudents, setTeamStudents] = useState([]);
    const [openNote, setOpenNote] = useState(false);
    const [openedData, setOpenedData] = useState({});
    const noteRef = React.useRef("");
    const remarkRef = React.useRef("");
    useEffect(() => {
        props.init_team(payload => {
            if (payload == -1) {
                setRender(false);
            } else setList([...payload]);
        });
        initMyStudens();
        props.init_team_students(setTeamSList);
    }, []);

    const initMyStudens = () => {
        props.init_my_students(setMySList);
    };

    useEffect(() => {}, [myStudents, myTeamStudents]);

    const setMySList = data => {
        setMyStudents(data);
    };
    const setTeamSList = data => {
        setTeamStudents(data);
    };
    const setIsOpenNoteOverlay = data => {
        const { note, remark } = data;

        setOpenNote(true);
        setOpenedData(data);
    };
    useEffect(() => {
        if (openNote) {
            setTimeout(() => {
                noteRef.current.value = openedData.note;
                remarkRef.current.value = openedData.remark;
            }, 100);
        }
    }, [openedData, openNote]);

    const changeMark = e => {
        e.preventDefault();
        axios
            .post("/admin/encadrant/changeMark", {
                note: noteRef.current.value,
                remark: remarkRef.current.value,
                groupe: openedData.groupe
            })
            .then(() => {
                initMyStudens();
                setOpenNote(false);
            })
            .catch(() => {});
    };

    ///pfe_system_v2
    return (
        <>
            <Drawer
                title="Modifier la note"
                isOpen={openNote}
                onClose={() => {
                    setOpenNote(false);
                }}
            >
                <div className="row justify-content-center">
                    <form onSubmit={changeMark} className={"col-12 col-md-8"}>
                        <HTMLTable className="col-12">
                            <tbody>
                                <tr>
                                    <td>Note :</td>
                                    <td>
                                        <InputGroup
                                            leftIcon={"numerical"}
                                            placeholder="Note"
                                            type={"number"}
                                            inputRef={noteRef}
                                            step={0.01}
                                            max={20}
                                            min={0}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remark :</td>
                                    <td>
                                        <InputGroup
                                            leftIcon={"annotation"}
                                            placeholder="Remark"
                                            type={"text"}
                                            inputRef={remarkRef}
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <Button
                                            type={"submit"}
                                            intent={Intent.SUCCESS}
                                        >
                                            Enregistrer
                                        </Button>
                                    </td>
                                </tr>
                            </tbody>
                        </HTMLTable>
                    </form>
                </div>
            </Drawer>
            <LazyLoader loading_id="team_jury" className={"w-100 pr-2"}>
                <div className="w-100">
                    <h2
                        style={{
                            color: "#878787"
                        }}
                    >
                        Mon équipe de jury :{" "}
                    </h2>

                    {render && list ? (
                        <HTMLTable striped className="w-100">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nom</th>
                                    <th>Prénom</th>
                                    <th>Email</th>
                                    <th>GSM</th>
                                </tr>
                            </thead>
                            <tbody>
                                {list.map(e => (
                                    <tr key={e.id}>
                                        <td>{e.id}</td>
                                        <td>{e.nom}</td>
                                        <td>{e.prenom}</td>
                                        <td>{e.email}</td>
                                        <td>{e.gsm}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </HTMLTable>
                    ) : (
                        <p>Vous n'etes pas encore integré dans un jury</p>
                    )}
                </div>
            </LazyLoader>
            <h2>Mes étudiants</h2>
            <LazyLoader loading_id={"my_students"}>
                {myStudents ? (
                    <HTMLTable striped className="w-100">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Matr</th>
                                <th>Membres</th>
                                <th>Encadrant Interne</th>
                                <th>Sujet et ville</th>
                                <th>Rapport</th>
                                <th>Note</th>
                            </tr>
                        </thead>
                        <tbody>
                            {myStudents.map(e => (
                                <tr key={e.id}>
                                    <td>{e.id}</td>
                                    <td>
                                        {e.filiere}-{e.id}
                                    </td>
                                    <td>
                                        {e.etudiants &&
                                            e.etudiants.map(et => (
                                                <p key={et.id}>
                                                    {et.nom} {et.prenom}
                                                </p>
                                            ))}
                                    </td>
                                    <td>
                                        {e.encadrant && (
                                            <p>
                                                {e.encadrant.nom}{" "}
                                                {e.encadrant.prenom}
                                            </p>
                                        )}
                                    </td>
                                    <td>
                                        <b>{e.sujet}</b>
                                        {e.ville_entreprise && (
                                            <span className={"pl-1"}>
                                                [{e.ville_entreprise}]
                                            </span>
                                        )}{" "}
                                    </td>
                                    <td>
                                        {e.all_reports &&
                                            e.all_reports.map(r => (
                                                <a
                                                    href={`/api/report/download/${r.find}`}
                                                    title={r.title}
                                                    target="_blank"
                                                >
                                                    <Button
                                                        fill
                                                        intent={
                                                            r.isFinal == 0
                                                                ? Intent.PRIMARY
                                                                : Intent.SUCCESS
                                                        }
                                                        icon={"download"}
                                                    >
                                                        {r.isFinal == 1
                                                            ? "Finale"
                                                            : "Avance"}
                                                    </Button>
                                                </a>
                                            ))}
                                    </td>
                                    <td>
                                        <Button
                                            intent={
                                                e.note_verified
                                                    ? Intent.SUCCESS
                                                    : Intent.PRIMARY
                                            }
                                            onClick={() => {
                                                setIsOpenNoteOverlay({
                                                    note: e.note,
                                                    remark: e.remark,
                                                    groupe: e.id
                                                });
                                            }}
                                        >
                                            {e.note ? e.note : 0}
                                        </Button>
                                        {e.note_verified ? (
                                            <>
                                                <br />
                                                <span>Vérifié</span>
                                            </>
                                        ) : null}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </HTMLTable>
                ) : (
                    <p></p>
                )}
            </LazyLoader>

            <h2>Les étudiants de mon équipe</h2>
            {console.log(myTeamStudents)}
            <LazyLoader loading_id={"team_students"}>
                {myTeamStudents != -1 ? (
                    <HTMLTable striped className="w-100">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Matr</th>
                                <th>Membres</th>
                                <th>Encadrant Interne</th>
                                <th>Sujet et ville</th>
                                <th>Rapport</th>
                            </tr>
                        </thead>
                        <tbody>
                            {myTeamStudents &&
                                myTeamStudents.map(list => {
                                    return list.map(e => (
                                        <tr key={e.id}>
                                            <td>{e.id}</td>
                                            <td>
                                                {e.filiere}-{e.id}
                                            </td>
                                            <td>
                                                {e.etudiants &&
                                                    e.etudiants.map(et => (
                                                        <p key={et.id}>
                                                            {et.nom} {et.prenom}
                                                        </p>
                                                    ))}
                                            </td>
                                            <td>
                                                {e.encadrant && (
                                                    <p>
                                                        {e.encadrant.nom}{" "}
                                                        {e.encadrant.prenom}
                                                    </p>
                                                )}
                                            </td>
                                            <td>
                                                <b>{e.sujet}</b>
                                                {e.ville_entreprise && (
                                                    <span className={"pl-1"}>
                                                        [{e.ville_entreprise}]
                                                    </span>
                                                )}{" "}
                                            </td>
                                            <td>
                                                {e.all_reports &&
                                                    e.all_reports.map(r => (
                                                        <a
                                                            href={`/api/report/download/${r.find}`}
                                                            title={r.title}
                                                            target="_blank"
                                                        >
                                                            <Button
                                                                fill
                                                                intent={
                                                                    r.isFinal ==
                                                                    0
                                                                        ? Intent.PRIMARY
                                                                        : Intent.SUCCESS
                                                                }
                                                                icon={
                                                                    "download"
                                                                }
                                                            >
                                                                {r.isFinal == 1
                                                                    ? "Finale"
                                                                    : "Avance"}
                                                            </Button>
                                                        </a>
                                                    ))}
                                            </td>
                                        </tr>
                                    ));
                                })}
                        </tbody>
                    </HTMLTable>
                ) : (
                    <p></p>
                )}
            </LazyLoader>
        </>
    );
};
export default HomePanelSupervisorRedux(HomePanelSupervisor);
