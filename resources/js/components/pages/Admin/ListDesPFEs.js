import React, {useEffect,useState} from 'react';
import ListDesPfe from "../Dashboard/ListDesPFE";
import {Filieres, FilieresTitle} from "../../../config/config";
import Select from "../../forms/Select";
import Button from "../../forms/Button";
import {ListDesPfEsRedux} from "../../../data/connect";



const ListDesPfEs = (props) => {
    const [filiere, setFiliere] = useState("GI");
    const updateSelect = (val)=>{
        window.open(`/generate_report/${val}`)
    }
    return (
        <div className="row w-100">
            {/* <div className={"row w-100"}>
                <Select
                    className="col-3"
                    tabIndex={"1"}
                    options={[
                        {value: "GI", label: FilieresTitle[Filieres.gi]},
                        {value: "GTR", label: FilieresTitle[Filieres.gtr]},
                    ]}
                    defaultValue={{
                        value: "GI",
                        label: FilieresTitle["GI"]
                    }}
                    onChange={(val) => {
                        updateSelect(val.value)
                    }}
                />
                <Button text={"Genere Rapport"}/>
            </div> */}
            <div className="w-100">
                <ListDesPfe/>
            </div>
        </div>
    );
}

export default ListDesPfEsRedux(ListDesPfEs)
