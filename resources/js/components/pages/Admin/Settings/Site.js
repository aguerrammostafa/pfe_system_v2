import React, { useEffect, useState } from "react";
import Button from "../../../forms/Button";
import { FormGroup, Divider } from "@blueprintjs/core";
import DatePickerCostume from "../../../forms/DatePicker";
import Alert from "../../../forms/Alert";

const Site = props => {
    useEffect(() => {}, [props.settings]);

    const onChange = (location, value) => {
        props.update_location(location, value);
    };

    return (
        <div className="w-100 p-2">
            <Alert message_id="admin_settings" />
            <div className="col-12 col-6">
                <h5>Dates importantes</h5>
                <Divider />
                <FormGroup inline label="Date limit de préinscription">
                    <DatePickerCostume
                        tabIndex="1"
                        value={props.settings.date_pre}
                        onChange={val => onChange("date_pre", val)}
                    />
                </FormGroup>
                <FormGroup inline label="Date limit de compléter les données">
                    <DatePickerCostume
                        tabIndex="2"
                        value={props.settings.date_com_don}
                        onChange={val => onChange("date_com_don", val)}
                    />
                </FormGroup>
                <FormGroup
                    inline
                    label="Date limite d’insertion du rapport d’avancement"
                >
                    <DatePickerCostume
                        tabIndex="3"
                        value={props.settings.date_ins_rap}
                        onChange={val => onChange("date_ins_rap", val)}
                    />
                </FormGroup>
            </div>
        </div>
    );
};
export default Site;
