import React, {useEffect,useState} from 'react'
import { Divider } from '@blueprintjs/core'
import AddSupervisor from '../Supervisor/AddSupervisor'
const Profile = (props)=>{
    return(
        <div className="w-100 pr-2">
            <div className="row">
                <AddSupervisor profileEdit={true} />
            </div>
        </div>
    )
}
export default Profile
