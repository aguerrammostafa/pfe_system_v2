import React, { useEffect, useState } from "react";
import { Tabs, Tab, Divider } from "@blueprintjs/core";
import Button from "../../../forms/Button";
import { AdminSettings } from "../../../../data/connect";

import Site from "./Site";
import Account from "./Account";
import Profile from "./Profile";
const Settings = props => {
    const [tab, setTab] = useState("site");
    const [currentPassword, setCurrentPassword] = useState("");
    const [password, setPassword] = useState("");

    useEffect(() => {
        if (!props.auth.admin) setTab("profile");
    }, [props.auth]);

    const handleTabChange = navId => {
        setTab(navId);
    };

    const canSave = () => {
        if (password.length > 0) return false;
        return props.settings.version && props.settings.version == 1;
    };

    const updateSettings = () => {
        if (props.settings.version != 1) {
            props.save_dates(props.settings);
        }
        if (password.length > 0) {
            props.update_password(currentPassword, password, () => {
                setPassword("");
                setCurrentPassword("");
            });
        }
    };

    return (
        <div className="w-100 pr-2">
            <div className="row justify-content-between align-items-baseline">
                <h4>Paramètres</h4>
                {props.auth.admin && (
                    <Button
                        onClick={updateSettings}
                        loading_id="admin_settings"
                        disabled={canSave()}
                        intent="primary"
                    >
                        Enregistrer
                    </Button>
                )}
            </div>
            <Divider />
            <Tabs
                animate={false}
                className={"w-100 dashboard-tabs"}
                onChange={handleTabChange}
                selectedTabId={tab}
            >
                {props.auth.admin && (
                    <Tab
                        id="site"
                        title="Les paramètres géneral"
                        panel={<Site {...props} />}
                    />
                )}
                {props.auth.admin && (
                    <Tab
                        id="account"
                        title="Les paramètres de compte"
                        panel={
                            <Account
                                password={password}
                                setPassword={setPassword}
                                currentPassword={currentPassword}
                                setCurrentPassword={setCurrentPassword}
                            />
                        }
                    />
                )}
                {!props.auth.admin && (
                    <Tab
                        id="profile"
                        title="Les informations personnelles"
                        panel={<Profile {...props} />}
                    />
                )}
            </Tabs>
        </div>
    );
};
export default AdminSettings(Settings);
