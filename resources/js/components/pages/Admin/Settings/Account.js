import React, { useEffect, useState } from "react";
import { FormGroup, InputGroup, Divider, Tooltip,Button, Intent  } from "@blueprintjs/core";
import Alert from "../../../forms/Alert";

const Account = props => {
    const [showPassword, setShowPassword] = useState(false);

    const lockButton = (
        <Tooltip
            content={`${showPassword ? "Cacher" : "Montrer"} le mot de passe`}
        >
            <Button
                icon={showPassword ? "eye-open" : "eye-off"}
                intent={Intent.WARNING}
                minimal={true}
                onClick={() => setShowPassword(!showPassword)}
            />
        </Tooltip>
    );

    const onChange = el => {
        let {name,value} = el.target
        if(name == "password")
        {
            props.setPassword(value)
        }
        else if(name == "current_password")
        {
            props.setCurrentPassword(value)
        }
    };

    return (
        <div className="w-100 p-2">
            <Alert message_id="admin_settings_account" />
            <div className="col-12 col-md-6">
                <h5>Changez votre mot de passe</h5>
                <Divider />
                <FormGroup inline label="Mot de passe actuel">
                    <InputGroup
                        tabIndex={"1"}
                        leftIcon={"lock"}
                        name="current_password"
                        placeholder="Mot de passe actuel"
                        rightElement={lockButton}
                        type={showPassword ? "text" : "password"}
                        value={props.currentPassword}
                        onChange={onChange}
                    />
                </FormGroup>
                <FormGroup inline label="Nouveau mot de passe">
                    <InputGroup
                        tabIndex={"2"}
                        leftIcon={"lock"}
                        placeholder="Mot de passe actuel"
                        rightElement={lockButton}
                        type={showPassword ? "text" : "password"}
                        value={props.password}
                        onChange={onChange}
                        name="password"
                    />
                </FormGroup>
            </div>
        </div>
    );
};
export default Account;
