import React, {useEffect,useState} from 'react';
import {Divider, HTMLTable, Alert as Dialog} from "@blueprintjs/core";
import Button from "../../../forms/Button";
import {ListSupervisorsRedux} from "../../../../data/connect";
import Alert from "../../../forms/Alert";
import LazyLoader from "../../../layouts/LazyLoader";

const ListSupervisors = (props) => {
    const [list, setList] = useState([]);
    const [allowDelete, setAllowDelete] = useState(false);
    const [changePasswordDialog, setChangePasswordDialog] = useState(null);
    useEffect(()=>{
        props.getList()
    },[]);

    const deleteEnc = (id) =>{
        props.deleteEncd(id)
    }
    const confirmPasswordReset = ()=>{
        props.resetPassword(changePasswordDialog,()=>{
            setChangePasswordDialog(null)
        })
    }

    return (
        <LazyLoader loading_id={"get_encd_list"}>
            <div className="w-100">
                <div className="row justify-content-between">
                    <Dialog
                        cancelButtonText={"Annuler"}
                        confirmButtonText={"Oui, réinitialiser le mot de passe\n"}
                        canEscapeKeyCancel
                        canOutsideClickCancel
                        intent={"warning"}
                        icon={"warning-sign"}
                        isOpen={changePasswordDialog != null}
                        onClose={()=>{
                            setChangePasswordDialog(null)
                        }}
                        onConfirm={confirmPasswordReset}
                    >
                        <div>
                            <p>
                                êtes-vous sûr de vouloir réinitialiser le mot de passe de {props.encadrant.map(e=>{
                                if(e.id == changePasswordDialog)
                                    return <b key={e.id}>{e.nom} {e.prenom}</b>
                            })} ?<br/>
                                <b>Le mot de passe sera réinitialisé à: <i>123456789</i></b>
                            </p>
                        </div>
                    </Dialog>
                    <h4>List des Encadrants</h4>
                </div>
                <Divider/>
                <HTMLTable striped className="w-100">
                    <thead>
                    <tr>
                        <th className="clickable">#</th>
                        <th>Nom</th>
                        <th>Email</th>
                        <th>User</th>
                        <th>Encadrer <small>(groupes)</small></th>
                        <th>Jury</th>
                        <th>Action <a onClick={()=>{
                            setAllowDelete(!allowDelete)
                        }}><small>{allowDelete?"(désactiver la suppression)":"(activer la suppression)"}</small></a> </th>
                    </tr>
                    </thead>
                    <tbody>
                    {props.encadrant.map(e=>{
                        return <tr key={e.id}>
                            <td>{e.id}</td>
                            <td>{e.nom} {e.prenom}</td>
                            <td>{e.email}</td>
                            <td>{e.admin.username}</td>
                            <td>{e.encadrer}</td>
                            <td>{e.jury > 0 ?e.jury:"No jury"}</td>
                            <td>
                                <Button loading_id={`enc_p_${e.id}`} onClick={()=>{
                                    setChangePasswordDialog(e.id)
                                }} intent={"warning"} minimal icon={"key"} />
                                <Button disabled={!allowDelete} icon={"trash"} loading_id={`deleteing${e.id}`} onClick={()=>deleteEnc(e.id)} minimal intent={"danger"} />
                            </td>
                        </tr>
                    })}

                    </tbody>
                </HTMLTable>
                <Alert message_id="delete_enc"/>
            </div>
        </LazyLoader>
    );
}

export default ListSupervisorsRedux(ListSupervisors)
