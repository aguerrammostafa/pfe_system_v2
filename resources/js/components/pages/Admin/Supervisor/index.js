import React, {useEffect,useState} from 'react';
import Button from "../../../forms/Button";
import {Divider} from "@blueprintjs/core";
import ListSupervisors from "./ListSupervisors";
import AddSupervisor from "./AddSupervisor";

const intent = {
    0:"default",
    1:"none"
}
const text = {
    0:"Ajouter Encadrant",
    1:"Retourner",
}
const icon = {
    0:"add",
    1:"chevron-left"
}
const Index = (props) => {
    const [page,setPage] = useState(0)
    const changePage = ()=>{
        if(page == 0)
        {
            setPage(1)
        }
        else if(page == 1)
        {
            setPage(0)
        }
    }

    return (
        <div>
            <Button icon={icon[page]} intent={intent[page]} onClick={changePage} text={text[page]}/>
            <Divider/>
            {
                page === 0?
                    <ListSupervisors/>
                    :
                    page == 1?
                        <AddSupervisor/>
                        :
                        null
            }
        </div>
    );
}

export default Index
