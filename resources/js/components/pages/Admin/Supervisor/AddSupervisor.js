import React, { useEffect, useRef, useState } from "react";
import Button from "../../../forms/Button";
import { HTMLTable, InputGroup, Tooltip, Intent } from "@blueprintjs/core";
import { AddSupervisorRedux } from "../../../../data/connect";
import Alert from "../../../forms/Alert";

const AddSupervisor = props => {
    const nomRef = useRef("");
    const prenomRef = useRef("");
    const gsmRef = useRef("");
    const emailRef = useRef("");
    const loginRef = useRef("");
    const passwordRef = useRef("");
    const [showPassword, setShowPassword] = useState(false);

    useEffect(() => {
        if (props.profileEdit) {
            props.initValues(data => {
                nomRef.current.value = data.nom;
                prenomRef.current.value = data.prenom;
                gsmRef.current.value = data.gsm;
                emailRef.current.value = data.email;
                loginRef.current.value = data.fonction;
                passwordRef.current.value = "";
            });
        }
    }, []);

    const resetForm = () => {
        nomRef.current.value = "";
        prenomRef.current.value = "";
        gsmRef.current.value = "";
        emailRef.current.value = "";
        loginRef.current.value = "";
        passwordRef.current.value = "";
    };
    const submitForm = e => {
        e.preventDefault();
        if (!props.profileEdit)
            props.addSupervisor(
                nomRef.current.value,
                prenomRef.current.value,
                gsmRef.current.value,
                emailRef.current.value,
                loginRef.current.value,
                passwordRef.current.value,
                resetForm
            );
        else {
            props.updateSuperVisor(
                nomRef.current.value,
                prenomRef.current.value,
                gsmRef.current.value,
                emailRef.current.value,
                passwordRef.current.value,
                loginRef.current.value,
                () => {}
            );
        }
    };
    const lockButton = (
        <Tooltip
            content={`${showPassword ? "Cacher" : "Montrer"} le mot de passe`}
        >
            <Button
                icon={showPassword ? "eye-open" : "eye-off"}
                intent={Intent.WARNING}
                minimal={true}
                onClick={() => setShowPassword(!showPassword)}
            />
        </Tooltip>
    );
    return (
        <div className="w-100">
            {!props.profileEdit && <h4>Ajouter Encadrant</h4>}
            <form onSubmit={submitForm} className="w-100">
                <HTMLTable className="col-12 col-md-6">
                    <tbody>
                        <tr>
                            <td>Nom :</td>
                            <td>
                                <InputGroup
                                    leftIcon={"user"}
                                    placeholder="Nom"
                                    type={"text"}
                                    inputRef={nomRef}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Prénom :</td>
                            <td>
                                <InputGroup
                                    leftIcon={"user"}
                                    placeholder="Prénom"
                                    type={"text"}
                                    inputRef={prenomRef}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>GSM :</td>
                            <td>
                                <InputGroup
                                    leftIcon={"phone"}
                                    placeholder="GSM"
                                    type={"text"}
                                    inputRef={gsmRef}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Email :</td>
                            <td>
                                <InputGroup
                                    leftIcon={"envelope"}
                                    placeholder="Email"
                                    type={"email"}
                                    inputRef={emailRef}
                                />
                            </td>
                        </tr>
                        {!props.profileEdit && (
                            <tr>
                                <td>Login :</td>
                                <td>
                                    <InputGroup
                                        leftIcon={"user"}
                                        placeholder="Login"
                                        type={"text"}
                                        inputRef={loginRef}
                                    />
                                </td>
                            </tr>
                        )}
                        {props.profileEdit && (
                            <tr>
                                <td>Fonction :</td>
                                <td>
                                    <InputGroup
                                        leftIcon={"book"}
                                        placeholder="Fonction"
                                        type={"text"}
                                        inputRef={loginRef}
                                    />
                                </td>
                            </tr>
                        )}
                        <tr>
                            <td>Password :</td>
                            <td>
                                <InputGroup
                                    leftIcon={"key"}
                                    placeholder="Password"
                                    type={"password"}
                                    inputRef={passwordRef}
                                    rightElement={lockButton}
                                    type={showPassword ? "text" : "password"}
                                />
                            </td>
                        </tr>
                    </tbody>
                </HTMLTable>
                <br />

                <Button
                    loading_id={"add_supr"}
                    type={"submit"}
                    intent={"primary"}
                    icon={"floppy-disk"}
                    text={"Enregistrer"}
                />
                <br />
                <Alert message_id={"add_supr"} />
            </form>
        </div>
    );
};

export default AddSupervisorRedux(AddSupervisor);
