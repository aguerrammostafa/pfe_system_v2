import React, { useEffect, useState } from "react";
import Container from "../../layouts/Container";
import { Callout, Card, Tab, Tabs } from "@blueprintjs/core";
import BreadcrumbCostume from "../../forms/Breadcrumb";
import Calendar from "../../shared/Calendar";
import AdminCalendar from "./Calendar";
import ListDesPfe from "../Dashboard/ListDesPFE";
import EditForm from "../Dashboard/EditForm";
import Report from "../Dashboard/Report";
import Button from "../../forms/Button";
import { AdminDashboardRedux } from "../../../data/connect";
import Supervisor from "./Supervisor";
import AffectSupervisor from "./AffectSupervisor";
import ListPfEs from "./ListPFEs";
import ListDesPFEs from "./ListDesPFEs";
import PfeTerms from "../Dashboard/PFETerms";
import UsefulDocuments from "../Dashboard/UsefulDocuments";
import HomePanel from "./HomePanel";
import Settings from "./Settings/";
import JuryIndex from "./JuryPanel/";
import HomePanelSupervisor from "./HomePanelSupervisor";
import ListDesNotes from "./ListDesNotes";
const Dashboard = props => {
    const [tab, setTab] = useState("home");
    useEffect(()=>{

    },[])
    const handleTabChange = navId => {
        setTab(navId);
    };

    return (
        <Container>
            <Card className="w-100">
                <div className="row pb-3">
                    <BreadcrumbCostume
                        items={[
                            {
                                href: "/",
                                text: "Accueil",
                                icon: "home"
                            },
                            {
                                text: "Admin",
                                icon: "dashboard"
                            }
                        ]}
                    />
                </div>
                <div className="row">
                    <Tabs
                        className={"w-100 dashboard-tabs"}
                        vertical
                        id="TabsExample"
                        onChange={handleTabChange}
                        selectedTabId={tab}
                    >
                        <Callout>
                            <span className="d-flex justify-content-center">
                                <span style={{ color: "#878080" }}>Menu</span>
                            </span>
                        </Callout>
                        <Tab
                            id={"home"}
                            title={"Accueil"}
                            panel={
                                props.auth.admin ? (
                                    <HomePanel />
                                ) : (
                                    <HomePanelSupervisor />
                                )
                            }
                        />
                        <Tab
                            id="tab2"
                            title="Modalités des PFE"
                            panel={<PfeTerms />}
                        />
                        <Tab
                            id="tab6"
                            title="Documents Utiles"
                            panel={<UsefulDocuments />}
                        />

                        {!props.auth.admin ? (
                            <Tab
                                key={"tab_Calendrier"}
                                id="tab1"
                                title="Calendrier"
                                panel={<Calendar />}
                            />
                        ) : [

                            <Tab
                                key={"tab_Calendrier"}
                                id="tab1"
                                title="Calendrier"
                                panel={<AdminCalendar />}
                            />,
                            <Tab
                                key={"tab_notes"}
                                id="notes"
                                title="List des notes"
                                panel={<ListDesNotes />}
                            />
                        ]}
                        <Tab
                            className="w-100"
                            id="tab3"
                            title="Liste des PFE"
                            panel={<ListDesPFEs />}
                        />
                        {props.auth.admin && (
                            <Tab
                                id="tab7"
                                title="Encadrants"
                                panel={<Supervisor />}
                            />
                        )}
                        {props.auth.admin && (
                            <Tab
                                id="tab8"
                                title="Attribuer Encadrants"
                                panel={<AffectSupervisor />}
                            />
                        )}
                        {props.auth.admin && (
                            <Tab
                                id="tab11"
                                title="Juries"
                                panel={<JuryIndex />}
                            />
                        )}
                        {/* {!props.auth.admin && <Tab className="w-100" id="tab10" title="Liste PFEs" panel={<ListPfEs/>}/>} */}
                        <Tab
                            id="tab_settings"
                            title="Paramètres"
                            panel={<Settings />}
                        />
                        <Button
                            minimal
                            onClick={() => {
                                window.localStorage.removeItem("token");
                                window.localStorage.removeItem("admin");
                                let isAdmin =
                                    window.auth.admin == false ||
                                    window.auth.admin == true;
                                window.location.href = "/admin";
                                window.auth = undefined;
                            }}
                            className="w-100"
                            intent="danger"
                            icon={"log-out"}
                        >
                            Déconnexion
                        </Button>
                    </Tabs>
                </div>
            </Card>
        </Container>
    );
};

export default AdminDashboardRedux(Dashboard);
