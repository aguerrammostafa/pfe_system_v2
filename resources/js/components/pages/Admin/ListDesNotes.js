import React, {useEffect, useState} from 'react';
import {Callout, Drawer, HTMLTable, InputGroup, Tab, Tabs} from "@blueprintjs/core";
import {Filieres, FilieresTitle} from "../../../config/config";
import axios from "../../../utils/axios"
import LazyLoader from "../../layouts/LazyLoader";
import Button from "../../forms/Button";
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";

const ListDesNotes = (props) => {
    const [tab, setTab] = useState("gi_tab")
    const [list, setList] = useState([])
    useEffect(() => {
        onChange()
    }, [])
    const onChange = ()=>{
        axios.get("/admin/groupes/")
            .then(({data}) => {
                setList(data)
            })
            .catch((ex) => {

            })
    }
    const handleTabChange = (navId) => {
        setTab(navId)
    };
    return (
        <Tabs animate={false} className="w-100" selectedTabId={tab} onChange={handleTabChange}>
            <Tab id={"gi_tab"} title={"Liste PFE G-Info"} panel={<FiliereList onChange={onChange} list={list} filiere={Filieres.gi}/>}/>
            <Tab id={"gtr_tab"} title={"Liste PFE GTR"} panel={<FiliereList onChange={onChange} list={list} filiere={Filieres.gtr}/>}/>
        </Tabs>
    );
}
export const FiliereList = ({filiere, ...props}) => {
    const [openNote, setOpenNote] = useState(false)
    const [openedData, setOpenedData] = useState({})

    const noteRef = React.useRef("")
    const remarkRef = React.useRef("")

    useEffect(()=>{

    },[props.list])
    useEffect(() => {
        if (openNote) {
            setTimeout(() => {
                noteRef.current.value = openedData.note
                remarkRef.current.value = openedData.remark
            }, 100)
        }
    }, [openedData, openNote])
    const initMyStudens = ()=>{
        props.onChange()
    }
    const setIsOpenNoteOverlay = (data) => {
        const {note, remark} = data

        setOpenNote(true)
        setOpenedData(data)
    }
    const acceptMark = (e) => {
        e.preventDefault()
        axios.post("/admin/groupes/accept/mark", {
            note: noteRef.current.value,
            remark: remarkRef.current.value,
            groupe: openedData.groupe
        })
            .then(() => {
                initMyStudens()
                setOpenNote(false)
            })
            .catch(() => {

            })
    }
    return <div className={"row w-100"}>
        <Drawer title="Acceptez la note" isOpen={openNote} onClose={() => {
            setOpenNote(false)
        }}>
            <div className="row justify-content-center">
                <form onSubmit={acceptMark} className={"col-12 col-md-8"}>
                    <HTMLTable className="col-12">
                        <tbody>
                        <tr>
                            <td>Note :</td>
                            <td>
                                <InputGroup
                                    leftIcon={"numerical"}
                                    placeholder="Note"
                                    type={"number"}
                                    inputRef={noteRef}
                                    step={0.01}
                                    max={20}
                                    min={0}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Remark :</td>
                            <td>
                                <InputGroup
                                    leftIcon={"annotation"}
                                    placeholder="Remark"
                                    type={"text"}
                                    inputRef={remarkRef}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <Button type={"submit"} intent={Intent.SUCCESS}>Accepter</Button>
                            </td>
                        </tr>
                        </tbody>
                    </HTMLTable>
                </form>
            </div>
        </Drawer>
        <Callout className="row justify-content-center">
            {FilieresTitle[filiere]}
        </Callout>
        <LazyLoader className="w-100" loading_id={"init_students_list_loading"}>
            <HTMLTable striped className="w-100">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Matr</th>
                    <th>Membres</th>
                    <th>Encadrant Interne</th>
                    <th>Note</th>
                    {/*<th>Rapport</th>*/}
                </tr>
                </thead>
                <tbody>
                {
                    props.list.length > 0 && props.list.map((e, index) => {
                        if (e.filiere.toUpperCase() === filiere) {
                            return <tr key={e.id}>
                                <td>{index + 1}</td>
                                <td>{e.filiere}-{e.id}</td>
                                <td>{e.etudiants && e.etudiants.map(et => (
                                    <p key={et.id}>{et.nom} {et.prenom}</p>
                                ))}</td>
                                <td>{
                                    e.encadrant && <p>{e.encadrant.nom} {e.encadrant.prenom}</p>
                                }</td>
                                <td>
                                    {
                                        e.note &&
                                        <Button
                                            onClick={()=>{
                                                setIsOpenNoteOverlay({
                                                    note: e.note,
                                                    remark: e.remark,
                                                    groupe: e.id
                                                })
                                            }}
                                            intent={e.note_verified==1 ? Intent.SUCCESS:Intent.DANGER}>{e.note}</Button>
                                    }

                                </td>
                            </tr>
                        }
                        return null
                    })
                }
                </tbody>
            </HTMLTable>
        </LazyLoader>
    </div>
}

export default ListDesNotes
