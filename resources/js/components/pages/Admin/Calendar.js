import React, {useEffect, useState,useRef} from 'react';
import Button from "../../forms/Button";
import {Divider, FormGroup, HTMLTable, InputGroup, Alert as Dialog} from "@blueprintjs/core";
import ListSupervisors from "./Supervisor/ListSupervisors";
import LazyLoader from "../../layouts/LazyLoader";
import {AdminCalendarRedux} from "../../../data/connect";
import DatePickerCostume from "../../forms/DatePicker";
import Alert from "../../forms/Alert";
import {ParseStyle, ToFullDateFormat} from "../../../utils/StyleParser";
import moment from "moment";

const Calendar = (props) => {
    const [calendarToEdit, setCalendarToEdit] = useState(null);
    useEffect(() => {
        props.list()
    }, [])

    const [page, setPage] = useState(0); //page 0-list, 1-add, 2- edit
    const changePage = (p)=>{
        if(p<=2 && p>=0)
        {
            setPage(p)
        }
    }
    const editCalendar = (id)=>{
        if(props.calendar)
        {
            let foundC = null
            for(let e of props.calendar)
            {
                if(e.id === id)
                {
                    foundC = e;
                    break;
                }
            }
            if(foundC != null)
            {
                setCalendarToEdit(foundC)
                setPage(2)
            }
        }

    }
    return <div>
        {
            page === 0?
                <CalendarList editCalendar={editCalendar} changePage={changePage} {...props}/>:
                page === 1?
                    <CalendarForm  changePage={changePage} {...props}/>
                    :
                    <CalendarForm {...calendarToEdit}  changePage={changePage} {...props} editMode />
        }
    </div>
}

const CalendarForm = (props)=>{
    const [date, setDate] = useState(props.at_date || new Date());
    const [description, setDescription] = useState(props.description || "");

    const submitForm = (e)=>{
        e.preventDefault()
        if(props.editMode)
        {
            if(props.id)
            {
                props.editCalendarAPI(props.id,date,description,()=>{
                    props.changePage(0)
                })
            }
        }
        else{
            props.addCalendar(date,description,()=>{
                setDescription("")
                setDate(new Date())
            })
        }
    }

    return <div className="w-100">
        <div className="w-100">
            <Button onClick={()=>props.changePage(0)} icon={"chevron-left"} text={"Retour"}/>
        </div>
        <div className="row justify-content-center">
            <form onSubmit={submitForm} className="col-12 p-sm-3 col-6">
                <div className="w-100">
                    <Alert message_id={"add_calendar"}/>
                </div>
                <FormGroup
                    label={"Date"}
                >
                    <DatePickerCostume
                        tabIndex={"1"}
                        value={date}
                        onChange={(val) => {
                            setDate(val)
                        }}
                    />
                </FormGroup>
                <FormGroup
                    label={"Description"}
                    labelInfo={"utilisez * text * pour le style gras, _text_ pour le style italique et ~ text ~ pour le style de suppression."}
                >
                    <InputGroup
                        onChange={(val)=>{
                            setDescription(val.target.value)
                        }}
                        value={description}
                        name={"description"}
                        icon={"calendar"}
                    />
                </FormGroup>
                <div className="w-100">
                    <p>Aperçu : </p>
                    <HTMLTable className={"w-100"}>
                        <thead>
                        <tr>
                            <th style={{minWidth:"120px"}}>Date</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                {ToFullDateFormat(date,true)}
                            </td>
                            <td>
                                <p dangerouslySetInnerHTML={{
                                    __html:ParseStyle(description)
                                }}/>
                            </td>
                        </tr>
                        </tbody>
                    </HTMLTable>
                </div>
                <Button loading_id={"add_calendar"} intent={"success"} text={"Enregistrer"} type={"submit"} />
            </form>
        </div>
    </div>
}

const CalendarList = (props) => {
    const [canDelete, setCanDelete] = useState(false);
    {/*<Dialog*/}
    {/*    cancelButtonText={"No"}*/}
    {/*    confirmButtonText={"Ok"}*/}
    {/*    isOpen={true}*/}
    {/*    onConfirm={()=>{*/}
    {/*        console.log("Confirmed")}*/}
    {/*    }*/}
    {/*/>*/}
    return <div className={"col-12"}>
        <Button icon={"add"} onClick={()=>props.changePage(1)} intent={"default"} text={"Ajouter un nouveau calendrier"}/>
        <LazyLoader loading_id={"calendar_list"} className="w-100 pr-3">
            <div className="w-100">
                <Alert message_id={"calendar_message"}/>
                <HTMLTable className="w-100">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Action <a onClick={
                            ()=>{
                                setCanDelete(!canDelete)
                            }
                        } > <small>{canDelete?"(désactiver la suppression)":"(activer la suppression)"}</small> </a></th>
                    </tr>
                    </thead>
                    <tbody>
                    {props.calendar && props.calendar.map(e => <tr key={e.id}>
                        <td>{ToFullDateFormat(e.at_date)}</td>
                        <td>
                            <p
                                dangerouslySetInnerHTML={{
                                    __html:ParseStyle(e.description)
                                }}
                            />
                        </td>

                        <td>
                            <Button onClick={()=>props.editCalendar(e.id)} minimal icon={"edit"} intent={"primary"}/>
                            <Button disabled={!canDelete} loading_id={`calendar_d_${e.id}`} onClick={()=>props.deleteCalendar(e.id)} minimal icon={"trash"} intent={"danger"}/>
                        </td>
                    </tr>)}
                    </tbody>
                </HTMLTable>
            </div>
        </LazyLoader>
    </div>
}
export default AdminCalendarRedux(Calendar)
