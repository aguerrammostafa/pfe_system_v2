import React, {useRef} from 'react'
import Container from '../../layouts/Container'
import Header from '../../shared/Header'
import Login from '../../shared/Login'
import {Card, InputGroup} from "@blueprintjs/core";
import Button from "../../forms/Button";
import {AdminAuthRedux} from '../../../data/connect'
import Alert from "../../forms/Alert";
import { Link } from 'react-router-dom';

const Index = (props) => {
    const usernameRef = useRef("");
    const passwordRef = useRef("");

    const login = (e) => {
        e.preventDefault()
        props.login(usernameRef.current.value, passwordRef.current.value)
    }

    return (
        <Container>
            <div className="row justify-content-center w-100">
                <Card className="col-12 col-md-4 p-4">
                    <form onSubmit={login} className="w-100">
                        <h2 align={"center"}>Admin</h2>
                        <InputGroup
                            leftIcon={"user"}
                            placeholder={"Nom d'utilisateur"}
                            inputRef={usernameRef}
                            type={"text"}
                            name={"username"}
                        />
                        <br/>
                        <InputGroup
                            leftIcon={"lock"}
                            placeholder="Mot de passe"
                            type={"password"}
                            inputRef={passwordRef}
                        />
                        <br/>
                        <div className="d-flex justify-content-end">
                            <Button type={"submit"} loading_id={"admin_auth"} intent={"primary"} text={"S'identifier"}/>
                        </div>
                        <Alert message_id={"admin_auth"}/>
                        <Link to="/" >Page d'accueil</Link>
                    </form>
                </Card>
            </div>
        </Container>
    )
}


export default AdminAuthRedux(Index)
