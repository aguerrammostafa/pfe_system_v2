import React, {useEffect, useState} from 'react';
import {ListPfEsRedux} from "../../../data/connect";
import {HTMLTable} from "@blueprintjs/core";
import Button from "../../forms/Button";

const ListPfEs = (props) => {
    const [list, setList] = useState([]);
    const start = (data) => {
        setList(data)
    }

    const ActionOnGroupes = (year, type="a") =>{

    }
    const submit = ()=>{
        window.open("/api/attestation/get?token="+localStorage.getItem("token"));
    }

    useEffect(() => {
        props.init(start)

    }, [])
    return (
        <div>
            <HTMLTable className="w-100">
                <thead>
                <tr>
                    <th>
                        Année Universitaire
                    </th>
                    <th>
                        Nom et Prénom
                    </th>
                    <th>
                        Intitulé de PFE
                    </th>
                    <th>
                        Selection
                    </th>
                </tr>
                </thead>
                <tbody>
                {
                    list && list.length>0 && list.map((e, i) => {
                        return <tr key={i}>
                            <td>
                                {e.year-1}-{e.year}
                            </td>
                            <td>
                                {e.data.map((da)=>(
                                    da.etudiants.map(et=>(
                                        <p key={et.id}>{et.nom} {et.prenom}</p>
                                    ))
                                ))}
                            </td>
                            <td>
                                {e.data.map((da,di)=>(
                                    <p key={di}>{da.sujet}</p>
                                ))}
                            </td>
                            <td>
                                <input onClick={()=>ActionOnGroupes(e.year)} type="checkbox" selected={true}/>
                            </td>
                        </tr>
                    })
                }
                </tbody>
            </HTMLTable>
            <div className="row justify-content-center">
                <Button onClick={submit} text={"Attestation"} className="ml-1"/>
                <Button text={"Pages de garde"}/>
            </div>
        </div>
    );
}

const CheckBox  = ()=>{
    return <div>
        <Button/>
    </div>
}

export default ListPfEsRedux(ListPfEs)
