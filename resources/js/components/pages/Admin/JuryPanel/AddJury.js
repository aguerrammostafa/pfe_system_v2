import React, {useEffect,useState} from 'react'
import {Dialog, InputGroup,FormGroup } from "@blueprintjs/core";
import Button from "../../../forms/Button";

const AddJury = ({ isOpen, setIsOpen, ...props }) => {
    const [value, setvalue] = useState("");
    const submit = e => {
        e.preventDefault();
        props.save(value, () => {
            setvalue("")
        });
    };
    return (
        <Dialog
            title="Ajouter un nouveau jury"
            onClose={() => setIsOpen(false)}
            isOpen={isOpen}
        >
            <div className="w-100 p-4">
                <form onSubmit={submit} className="col">
                    <FormGroup>
                        <InputGroup
                            value={value}
                            onChange={e => setvalue(e.target.value)}
                            placeholder="Titre"
                        />
                    </FormGroup>
                    <Button
                        type="submit"
                        intent="success"
                        icon="floppy-disk"
                        text="Enregistrer"
                        loading_id="add_jury_loading"
                    />
                </form>
            </div>
        </Dialog>
    );
};
export default AddJury
