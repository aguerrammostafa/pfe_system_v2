import React, { useEffect, useState } from "react";
import LazyLoader from "../../../layouts/LazyLoader";
import { JuryRedux } from "../../../../data/connect";
import Button from "../../../forms/Button";
import Alert from "../../../forms/Alert";
import { Icon, Tooltip, Dialog, InputGroup } from "@blueprintjs/core";
import { FormGroup } from "@blueprintjs/core/lib/cjs";
import JuryCard from "./JuryCard"
import AddJury from "./AddJury"
import AffectJury from "./AffectJury"
const JuryIndex = props => {
    const [isOpen, setIsOpen] = useState(false);
    const [isOpenAffect, setIsOpenAffect] = useState(false);
    const [selectedName, setSelectedName] = useState(null);
    const [selectedId, setSelectedId] = useState(null);
    useEffect(() => {
        props.init();
    }, []);
    return (
        <div className="col-12 pr-2">
            <Button
                onClick={() => {
                    setIsOpen(true);
                }}
                icon="add"
                intent="primary"
                text="Ajouter un nouveau jury"
            />
            <AddJury
                save={props.addJury}
                isOpen={isOpen}
                setIsOpen={setIsOpen}
            />
            <Alert message_id="jury_result" />
            <AffectJury
                isOpen={isOpenAffect}
                setIsOpen={setIsOpenAffect}
                name={selectedName}
                idEnc={selectedId}
                list={props.jury}
                affectJury={props.affectJury}
            />
            <LazyLoader loading_id="jury_list" className={"w-100"}>
                <div className="w-100">
                    {props.jury.map(el => (
                        <JuryCard
                            setIsOpenAffect={setIsOpenAffect}
                            setIsOpen={setIsOpenAffect}
                            setSelectedName={setSelectedName}
                            setSelectedId={setSelectedId}
                            removeEncadrant={props.removeEncadrant}
                            deleteJury={props.removeJury}
                            key={el.id}
                            {...el}
                        />
                    ))}
                </div>
            </LazyLoader>
        </div>
    );
};


export default JuryRedux(JuryIndex);
