import React, {useEffect,useState} from 'react'
import { Icon, Tooltip, Dialog, InputGroup } from "@blueprintjs/core";
import Button from "../../../forms/Button";

const JuryCard = props => {
    let grpCount = [];
    return (
        <div className="col-12 pr-2">
            <div
                className="d-flex w-100"
                style={{
                    border: props.id != 0 ? "2px solid #e6d1d1":"3px solid #cb2020",
                    margin: "4px",
                    padding: "4px"
                }}
            >
                <h4 className="col-2">{props.title}</h4>
                <ul
                    style={{
                        listStyle: "none"
                    }}
                    className="col"
                >
                    {props.encadrants.map(e => {
                        grpCount.push(e.groupes_count);
                        return (
                            <li key={e.id} className="d-inline">
                                {props.id != 0 ? (
                                    <Button
                                        icon="trash"
                                        minimal
                                        onClick={() => {
                                            props.removeEncadrant(
                                                e.id,
                                                props.id
                                            );
                                        }}
                                        intent="danger"
                                        loading_id={`remove_btn_${e.id}_${props.id}`}
                                    />
                                ) : (
                                    <Button
                                        icon="pin"
                                        minimal
                                        onClick={() => {
                                            // props.removeEncadrant(
                                            //     e.id,
                                            //     props.id
                                            // );
                                            props.setSelectedId(e.id);
                                            props.setSelectedName(
                                                `${e.nom} ${e.prenom}`
                                            );
                                            props.setIsOpenAffect(true);
                                        }}
                                        intent="primary"
                                        loading_id={`assing_btn_${e.id}_${props.id}`}
                                    />
                                )}
                                {e.nom} {e.prenom}
                                &nbsp;
                                <Tooltip content="Total des groupes encadré par ce professeur">
                                    <span
                                        style={{
                                            border: "1px solid #604f4f",
                                            padding: "4px 8px",
                                            color: "white",
                                            backgroundColor: "#726a6a"
                                        }}
                                    >
                                        {e.groupes_count}
                                    </span>
                                </Tooltip>
                            </li>
                        );
                    })}
                </ul>
            </div>
            <div className="w-100 d-flex justify-content-between">
                <div>
                    {props.id != 0 && (
                        <Button
                            loading_id={`remove_jury_${props.id}`}
                            text="Supprimer jury"
                            icon="trash"
                            intent="danger"
                            minimal
                            onClick={() => {
                                props.deleteJury(props.id);
                            }}
                        />
                    )}
                </div>

                <p>
                    Total des groupes :{" "}
                    <b>
                        {grpCount.length > 0
                            ? grpCount.reduce((a, b) => a + b)
                            : 0}
                    </b>
                </p>
            </div>
        </div>
    );
};
export default JuryCard
