import React, { useEffect, useState } from "react";
import { Dialog } from "@blueprintjs/core";
import Button from "../../../forms/Button";
import Select from "../../../forms/Select";

const AffectJury = ({ isOpen, setIsOpen, ...props }) => {
    const [juryID, setJuryID] = useState(0)
    const submit = (e)=>{
        e.preventDefault()
        if(juryID>0)
        {
            props.affectJury(props.idEnc,juryID,()=>{
                setIsOpen(false)
            })
        }
        else{
            setIsOpen(false)
        }
    }
    return (
        <Dialog
            title={`Ajouter ${props.name} à un jury`}
            isOpen={isOpen}
            onClose={() => setIsOpen(false)}
        >
            <div className="d-flex justify-content-center">
                <form onSubmit={submit} className="w-100 p-4">
                    <Select
                        options={[
                            ...props.list.map(e => ({
                                value: e.id,
                                label: e.id == 0?"No jury":`${e.title} (${e.encadrants.length})`
                            }))
                        ]}
                        defaultValue={{
                            value: 0,
                            label: "Sélectionner un jury"
                        }}
                        onChange={(val)=>{
                            setJuryID(val.value)
                            setSelectedValue(val)
                        }}
                    />
                    <br/>
                    <Button
                        type="submit"
                        intent="success"
                        icon="floppy-disk"
                        text="Enregistrer"
                        loading_id="affect_jury_loading"
                    />
                </form>
            </div>
        </Dialog>
    );
};
export default AffectJury;
