import React, { useEffect, useState } from "react";
import { HomePanelRedux } from "../../../data/connect";
import LazyLoader from "../../layouts/LazyLoader";
import { FilieresTitle } from "../../../config/config";
import { HTMLTable } from "@blueprintjs/core";
const HomePanel = props => {
    const [list, setList] = useState([]);
    const [totalEncadrant, setTotalEncadrant] = useState(0);
    const [groupes, setGroupes] = useState([]);
    const [groupesNon, setGroupesNon] = useState([]);
    const [notes_not_verified,setnotes_not_verified] = useState([])
    const [notes_not_set,setnotes_not_set] = useState([])
    const [note_avg,setnote_avg] = useState([])

    useEffect(() => {
        props.fetch(payload => {
            let count = new Map();
            payload.etudiants.map(e => {
                if(e.groupe == null)
                    return;
                let f = e.groupe.filiere;
                if (count.has(f)) {
                    count.set(f, count.get(f) + 1);
                } else {
                    count.set(f, 1);
                }
            });

            setTotalEncadrant(payload.encadrants);
            setGroupes(payload.groupes);
            setGroupesNon(payload.groupes_non);
            setnotes_not_verified(payload.notes_not_verified)
            setnotes_not_set(payload.notes_not_set)
            setnote_avg(payload.note_avg)
            //set total des etudiants
            let l = [];
            count.forEach((v, k) => {
                l = [
                    ...l,
                    {
                        filiere: k,
                        total: v
                    }
                ];
            });
            setList([...l]);
        });
    }, []);

    return (
        <LazyLoader loading_id="admin_statistics" className={"w-100 pr-2"}>
            <div className="row">
                <HTMLTable striped className="w-100">
                    <tbody>
                        <tr>
                            <td>
                                <b>Total des étudiants</b>
                            </td>
                            <td>
                                {list.length > 0 ? (
                                    list.map((e, i) => {
                                        return (
                                            <p key={i}>
                                                <i>
                                                    {FilieresTitle[e.filiere]}
                                                </i>{" "}
                                                : {e.total}{" "}
                                            </p>
                                        );
                                    })
                                ) : (
                                    <p>il n'y a pas de données à afficher</p>
                                )}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Total des groupes activés</b>
                            </td>
                            <td>
                                {groupes.length > 0 ? (
                                    groupes.map((e, i) => {
                                        return (
                                            <p key={i}>
                                                <i>
                                                    {FilieresTitle[e.filiere]}
                                                </i>{" "}
                                                : {e.total}{" "}
                                            </p>
                                        );
                                    })
                                ) : (
                                    <p>il n'y a pas de données à afficher</p>
                                )}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Total des groupes non activés</b>
                            </td>
                            <td>
                                {groupesNon.length > 0 ? (
                                    groupesNon.map((e, i) => {
                                        return (
                                            <p key={i}>
                                                <i>
                                                    {FilieresTitle[e.filiere]}
                                                </i>{" "}
                                                : {e.total}{" "}
                                            </p>
                                        );
                                    })
                                ) : (
                                    <p>il n'y a pas de données à afficher</p>
                                )}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Total des marques non vérifiées</b>
                            </td>
                            <td>
                                {notes_not_verified.length > 0 ? (
                                    notes_not_verified.map((e, i) => {
                                        return (
                                            <p key={i}>
                                                <i>
                                                    {FilieresTitle[e.filiere]}
                                                </i>{" "}
                                                : {e.totle}{" "}
                                            </p>
                                        );
                                    })
                                ) : (
                                    <p>il n'y a pas de données à afficher</p>
                                )}
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <b>Total des notes non insérées</b>
                            </td>
                            <td>
                                {notes_not_set.length > 0 ? (
                                    notes_not_set.map((e, i) => {
                                        return (
                                            <p key={i}>
                                                <i>
                                                    {FilieresTitle[e.filiere]}
                                                </i>{" "}
                                                : {e.totle}{" "}
                                            </p>
                                        );
                                    })
                                ) : (
                                    <p>il n'y a pas de données à afficher</p>
                                )}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Moyenne des notes</b>
                            </td>
                            <td>
                                {note_avg.length > 0 ? (
                                    note_avg.map((e, i) => {
                                        return (
                                            <p key={i}>
                                                <i>
                                                    {FilieresTitle[e.filiere]}
                                                </i>{" "}
                                                : {e.avg}{" "}
                                            </p>
                                        );
                                    })
                                ) : (
                                    <p>il n'y a pas de données à afficher</p>
                                )}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Total des encadrants</b>
                            </td>
                            <td>{totalEncadrant}</td>
                        </tr>
                    </tbody>
                </HTMLTable>
            </div>
        </LazyLoader>
    );
};

export default HomePanelRedux(HomePanel);
