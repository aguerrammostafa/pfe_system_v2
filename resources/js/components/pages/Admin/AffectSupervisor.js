import React, {useEffect, useState} from 'react';
import {AffectSupervisorRedux} from "../../../data/connect";
import {Divider, FormGroup, HTMLTable} from "@blueprintjs/core";
import Button from "../../forms/Button";
import {Filieres, FilieresTitle} from "../../../config/config";
import Select from "../../forms/Select";
import Alert from "../../forms/Alert";

const montrerList = [
    {value: "N", label: "Toutes les filiéres"},
    {value: "GI", label: FilieresTitle[Filieres.gi]},
    {value: "GTR", label: FilieresTitle[Filieres.gtr]}
]
const sortBy = [
    {value:"nom",label:"Nom"},
    {value:"prenom",label:"Prenom"},
    {value:"nom",label:"Nom"},
]
const AffectSupervisor = (props) => {
    const [encadrants, setEncadrants] = useState([]);
    const [itemsToUpdate, setItemsToUpdate] = useState([]);
    const [superVisorList, setSuperVisorList] = useState({value:-1,label:"Toutes les encadrants"});
    const [showOnly, setShowOnly] = useState(0) //0-all 1-GI 2-GTR

    useEffect(() => {
        let nEnc = [{
            value: 0,
            label: "No encadrant"
        }]
        nEnc = [...nEnc, ...props.encadrants.map(e => ({
            value: e.id,
            label: `${e.nom} ${e.prenom}`
        }))]
        setEncadrants(nEnc)
    }, [props.encadrants, props.students_list]);

    function updateProps(value, groupe_id) {
        setItemsToUpdate((prev) => ([
            ...prev,
            {
                groupe_id,
                supervisor: value
            }
        ]))
    }

    const callback = () => {
        setItemsToUpdate([])
    }

    const affectEncd = () => {
        if (itemsToUpdate.length > 0)
            props.affectEnc(itemsToUpdate, callback)
    }

    return (
        <div className="w-100">
            <div className="row justify-content-between align-items-baseline pr-2">
                <h4>Affecter Encadrant</h4>
                <Button disabled={itemsToUpdate.length <= 0} onClick={affectEncd} loading_id={"affect_enc"}
                        intent={"primary"} text={"Enregistrer"}/>
                <Alert message_id={"affect_enc"}/>
            </div>
            <Divider/>
            <div className="row align-items-center">
                <div className="col-12 col-md-6 p-2">
                    <FormGroup
                        label={"Filtrer par filiére"}
                    >
                        <Select
                            className="col-12"
                            options={montrerList}
                            value={montrerList[showOnly]}
                            defaultValue={montrerList[showOnly]}
                            onChange={(val) => {
                                montrerList.map((e,i)=>{
                                    if(e.value == val.value)
                                    {
                                        setShowOnly(i)
                                    }
                                })
                            }}
                        />
                    </FormGroup>
                </div>
                <div className="col-12 col-md-6 p-2">
                    <FormGroup
                        label={"Par encadrant"}
                    >
                        <Select
                            className="col-12"
                            options={[
                                {value:-1,label:"Toutes les encadrants"},
                                ...encadrants
                            ]}
                            value={superVisorList}
                            defaultValue={superVisorList}
                            onChange={(val) => {
                                setSuperVisorList(val)
                            }}
                        />
                    </FormGroup>
                </div>
            </div>
            <HTMLTable striped className="w-100">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Matricule</th>
                    <th>Membres</th>
                    <th>Encadrant</th>
                    <th>Sujet</th>
                </tr>
                </thead>
                <tbody>
                {props.students_list.map(e => {
                    if((showOnly === 1 || showOnly === 2) && e.filiere != montrerList[showOnly].value)
                        return null;
                    if(superVisorList.value != -1 && e.encadrant_id != superVisorList.value)
                        return null;
                    return <tr key={e.id}>
                        <td>{e.id}</td>
                        <td>{e.filiere}-{e.id}</td>
                        <td>{e.etudiants.map(el => (
                            <p key={el.id}>{el.nom} {el.prenom}</p>
                        ))}</td>
                        <td style={{minWidth: "200px"}}>
                            {
                                encadrants.length > 0 &&
                                <Select
                                    tabIndex={"1"}
                                    options={encadrants}
                                    defaultValue={{
                                        value: e.encadrant ? e.encadrant.id : encadrants[0].value,
                                        label: e.encadrant ? (`${e.encadrant.nom} ${e.encadrant.prenom}`) : encadrants[0].label
                                    }}
                                    onChange={(val) => {
                                        updateProps(val.value, e.id)
                                    }}
                                />
                            }
                        </td>
                        <td>{e.sujet}</td>
                    </tr>
                })}
                </tbody>
            </HTMLTable>
        </div>
    );
}

export default AffectSupervisorRedux(AffectSupervisor)
