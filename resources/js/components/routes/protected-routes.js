import React,{useState, useEffect} from "react"
import {Route, Redirect} from 'react-router-dom'

/**
 * Protected routes
 * @param login, 0: requires not to login, 1: requires login, 2: route that works every where
 * @param auth
 * @param component
 * @param path
 * @param exact
 * */
export const Permissions = {
    "loggedin":1,
    "not_loggedin":0,
    "all":2,
}
const ProtectedRoute = ({login, auth, component, path, exact, adminRoute}) => {
    const [loggedin, setLoggedin] = useState(false)
    useEffect(() => {
        if (auth && auth.access_token) {
            setLoggedin(true)
        } else {
            setLoggedin(false)
        }
    }, [auth && auth.access_token])

    if (!loggedin && login === Permissions.loggedin) {
        if(adminRoute)
        {
            return <Redirect to={"/admin"}/>
        }
        else{
            return <Redirect to={"/"}/>
        }
    } else if (loggedin && login === Permissions.not_loggedin) {
        if(adminRoute)
        {
            return < Redirect to={"/admin/dashboard"}/>
        }
        else{
            if(auth.admin)
            {
                return < Redirect to={"/admin/dashboard"}/>
            }
            else{
                return < Redirect to={"/dashboard"}/>
            }
        }
    } else {
        return <Route component={component} path={path} exact={exact}/>
    }
}
export default ProtectedRoute;
