import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NotFound from "../pages/NotFound";
import { AppRedux } from "../../data/connect";
import ProtectedRoute, { Permissions } from "./protected-routes";
import Main from "../pages/Main";
import Dashboard from "../pages/Dashboard";
import Signup from "../pages/Signup/";
import Admin from "../pages/Admin/";
import AdminDashboard from "../pages/Admin/Dashboard";
import ResetPassword from "../pages/ResetPassword/";
import RecoverPassword from "../pages/ResetPassword/RecoverPassword";
import { ProgressBar, Spinner } from "@blueprintjs/core";

let tasks = 0;

const Index = AppRedux(props => {
    const [firstTime, setFirstTime] = useState(false);
    //this will be set to true when checking if the token is valid
    const [ready, setReady] = useState(false);

    /**
     * This method will be called when done checking the token if it's valid
     * */
    const callback = () => {
        taskDone("checkToken");
    };
    const taskDone = (title = "") => {
        tasks++;
        if (tasks == 3 && (!props.auth || !props.auth.access_token)) {
            setReady(true);
        } else if (tasks == 4 && props.auth && props.auth.access_token) {
            setReady(true);
        }
    };
    useEffect(() => {
        if (!firstTime) {
            props.bootstrap();
            props.init_students_list(() => {
                taskDone("init_students_list");
            });
            props.init_settings(() => {
                taskDone("init_settings");
            });
            props.init_calendar(() => {
                taskDone("init_calendar");
            });
            setFirstTime(true);
        }
        if (props.auth && props.auth.access_token) {
            window.auth = props.auth;
            props.checkToken(
                callback,
                props.auth.admin ||
                    window.location.pathname.startsWith("/admin")
            );
        }
    }, [
        props.auth && props.auth.access_token,
        props.router.location.pathname,
        tasks
    ]);
    if (ready) {
        return (
            <div

            >
                <Router>
                    <Switch>
                        <ProtectedRoute
                            login={Permissions.not_loggedin}
                            auth={props.auth}
                            component={Main}
                            path={"/"}
                            exact={true}
                        />
                        <ProtectedRoute
                            login={Permissions.not_loggedin}
                            auth={props.auth}
                            component={Signup}
                            path={"/signup"}
                            exact={false}
                        />
                        <ProtectedRoute
                            login={Permissions.loggedin}
                            auth={props.auth}
                            component={Dashboard}
                            path={"/dashboard"}
                            exact={false}
                        />
                        <ProtectedRoute
                            login={Permissions.not_loggedin}
                            component={ResetPassword}
                            path={"/reset"}
                            exact={true}
                        />
                        <ProtectedRoute
                            login={Permissions.not_loggedin}
                            component={RecoverPassword}
                            path={"/reset/:token/:email"}
                            exact={false}
                        />
                        <ProtectedRoute
                            login={Permissions.not_loggedin}
                            auth={props.auth}
                            component={Admin}
                            path={"/admin"}
                            exact={true}
                            adminRoute={true}
                        />
                        <ProtectedRoute
                            login={Permissions.loggedin}
                            auth={props.auth}
                            component={AdminDashboard}
                            path={"/admin/dashboard"}
                            exact={true}
                            adminRoute={true}
                        />
                        <Route component={NotFound} />
                    </Switch>
                </Router>
            </div>
        );
    } else {
        return (
            <div className="row justify-content-center align-items-center vh-100 w-100">
                <div>
                    <h3>S'il vous plaît, attendez</h3>
                    <Spinner />
                </div>
            </div>
        );
    }
});

export default Index;
