import React, {Component} from 'react';
import {Provider} from 'react-redux'
import ReactDOM from 'react-dom';
import Store from '../data/store'
import sagaMiddleware from '../data/sagas/sagas'
import Sagas from '../data/sagas'
import Routes from './routes'
import { ConnectedRouter } from 'connected-react-router'
import history from "./routes/history";
sagaMiddleware.run(Sagas)

/*
    NOTHING TO TOUCH HERE, GO AWAY
* */
export default class App extends Component {
    render() {
        return (
            <Provider store={Store}>
                <ConnectedRouter history={history}>
                    <Routes/>
                </ConnectedRouter>
            </Provider>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(<App/>, document.getElementById('root'));
}
