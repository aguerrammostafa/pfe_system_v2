import React, {useEffect, useState} from 'react';
import {Breadcrumb, Breadcrumbs, Button, Icon} from "@blueprintjs/core";
import {BreadCrumbRedux} from "../../data/connect";
import {Redirect} from 'react-router-dom'
import PropTypes from 'prop-types';

/**
 * @param items
 * */
const BreadcrumbCostume = ({items,...props}) => {
    const [list,setList] = useState(null)
    useEffect(() => {

    },[])

    return <div className="row align-items-center">
        {items.map((e,i)=><Btn push={props.push} key={i} {...e}/>)}
    </div>
}
BreadcrumbCostume.propTypes = {
    items:PropTypes.arrayOf(
        PropTypes.exact({
            href:PropTypes.string,
            icon:PropTypes.string,
            text:PropTypes.string.isRequired,
        })
    )
}
/**
 * @param href
 * @param icon
 * @param text
 * @param push
 * */
const Btn = ({href,icon,text,push})=>{
    const [send,setSend] = useState(null)
    useEffect(()=>{

    },[send])

    let arrow = null
    let func = null
    let props = {
        minimal:true,
        text
    }
    if(href)
    {
        arrow = <Icon color={"#98999e"} icon={"caret-right"}/>
        props.onClick = ()=>setSend(<Redirect to={href}/>)
    }
    else{
        props.disabled = true
    }
    if(icon)
    {
        props.icon = <Icon icon={icon}/>
    }
    return <><Button {...props}/> {arrow} {send}</>

}
export default BreadCrumbRedux(BreadcrumbCostume)
