import React, { useEffect, useState } from "react";
import { AlertRedux } from "../../data/connect";
import { Callout, Toast, Toaster } from "@blueprintjs/core";

/**
 * @param message_id
 * */
const Alert = props => {
    const [message, setMessage] = useState(null);

    const unsetMessage = () => {
        setMessage(null);
        props.unsetMessage(props.message_id);
    };
    useEffect(() => {
        if (props.message[props.message_id]) {
            let toast = false;
            let msg = props.message[props.message_id];
            let type = "success";
            if (msg.type === "error") {
                type = "danger";
            } else if (msg.type.indexOf("toast_") === 0) {
                toast = true;
                let sp = msg.type.split("_");
                switch (sp[1]) {
                    case "danger":
                        type = "danger";
                        break;
                    case "warning":
                        type = "warning";
                        break;
                    default:
                        type = "success";
                }
            }
            let content = props.message[props.message_id].message;
            setMessage(
                <Callout intent={type}>
                    <div dangerouslySetInnerHTML={{ __html: content }} />
                    <span onClick={unsetMessage} className={"alert-close-sign"}>
                        &times;
                    </span>
                </Callout>
            );
            if (toast) {
                setMessage(
                    <Toaster position={"top-left"}>
                        <Toast
                            timeout={10000}
                            onDismiss={d => {
                                unsetMessage();
                            }}
                            intent={type}
                            icon={type == "success" ? "tick" : type =="danger"?"error":"warning-sign"}
                            message={
                                <div
                                    dangerouslySetInnerHTML={{
                                        __html: content
                                    }}
                                />
                            }
                        />
                    </Toaster>
                );
            }
        }
    }, [props.message]);

    return <>{message}</>;
};

export default AlertRedux(Alert);
