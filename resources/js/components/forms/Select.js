import React, {useEffect,useState} from 'react';
import Select from 'react-select'

const customStyles = {
    option: (provided, state) => ({
        ...provided,
        padding: 5,
    }),
    container: (provided, state) => ({
        ...provided,
        height: "30px",
        minHeight: "30px"
    }),
    control: (provided, state) => ({
        ...provided,
        height: "30px",
        minHeight: "30px",

    }),
    singleValue: (provided, state) => {
        const opacity = state.isDisabled ? 0.5 : 1;
        const transition = 'opacity 300ms';

        return {...provided, opacity, transition};
    },
    indicatorsContainer: (provided, state) => ({
        ...provided, height: "30px"
    }),
    menu: (provided, state) => ({
        ...provided, zIndex: 11111
    })
}
/**
 * @param options
 * @param defaultValue
 * @param onChange
 * */
const CostumeSelect = (props) => {
    const [value,setValue] = useState({});
    useEffect(()=>{
        setValue(props.defaultValue)
    },[])
    return (
        <Select
            {...props}
            value={value}
            styles={customStyles}
            isSearchable={true}
            isDisabled={props.disabled}
            onChange={(val)=>{
                setValue(val)
                props.onChange && props.onChange(val)
            }}
        />
    );
}

export default CostumeSelect
