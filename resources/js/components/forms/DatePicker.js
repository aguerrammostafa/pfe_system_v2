import React, {useEffect,useState} from 'react';
import {Popover} from "@blueprintjs/core";
import {DatePicker} from "@blueprintjs/datetime";
import Button from "./Button";
import moment from "moment"
/**
 * @param onChange
 * @param value
 * */
const DatePickerCostume = ({onChange,...props}) => {
    const [open,setOpen] = useState(false);
    const [value,setValue] = useState(null);
    useEffect(()=>{
        if(props.value)
        {
            setValue(formatDate(props.value))
        }
    })
    const handleChange = (val)=>{
        let vToShow = formatDate(val);
        setValue(vToShow);
        onChange(val)
    };
    const formatDate = (date)=>{
        return  moment(date).format("DD-M-YYYY")
    }
    return (
        <Popover
            defaultIsOpen={false}
            isOpen={open}
            content={<DatePicker
                highlightCurrentDay
                showActionsBar
                onChange={handleChange}
                maxDate={new Date(`12-12-${new Date().getFullYear()+5}`)}
                minDate={new Date(`12-12-${new Date().getFullYear()-5}`)}
                value={props.value && new Date(props.value)}
            />}
            onInteraction={(v)=>{
                setOpen(v)
            }}
        >
            <Button disabled={props.disabled} intent={"primary"} icon={"calendar"} text={`Date ${value?value:""}`}/>
        </Popover>
    );
}

export default DatePickerCostume
