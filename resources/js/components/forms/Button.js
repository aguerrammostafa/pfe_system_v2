import React,{useEffect} from 'react';
import {Button as Btn} from '@blueprintjs/core'
import {ButtonRedux} from "../../data/connect";
const Button = ({loading_id,children,dispatch,loading,...rest}) => {
    useEffect(()=>{

    },[loading])
    let activeLoading = false
    if(loading[loading_id])
    {
        activeLoading = loading[loading_id]
    }
    return (
        <Btn loading={activeLoading} {...rest}>
            {children}
        </Btn>
    );
}

export default ButtonRedux(Button)
