import React, {useEffect} from 'react';
import Logo from "../../../img/ensa_logo.png"
import {Link} from "react-router-dom";
import {Callout, Card} from "@blueprintjs/core";
const Header = (props) => {
    return (
        <Link className="p-4"  to={location.pathname.startsWith("/admin")?"/admin":"/"}>
            <Card className="row align-items-center justify-content-center">
                <img src={Logo} height={64}/>
                <h4>Département Informatique | Réseaux et Télécommunications</h4>
            </Card>
        </Link>
    );
}

export default Header
