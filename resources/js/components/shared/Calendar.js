import React, {useEffect} from 'react';
import {HTMLTable} from "@blueprintjs/core";
import {CalendarRedux} from "../../data/connect";
import {ParseStyle, ToFullDateFormat} from "../../utils/StyleParser";
import Button from "../forms/Button";
import LazyLoader from "../layouts/LazyLoader";

const Calendar = (props) => {
    useEffect(()=>{

    },[props.calendar]);

    return (
        <LazyLoader loading_id="calendar_list" className="w-100">
            <HTMLTable className="w-100">
                <thead>
                <tr>
                    <th style={{minWidth:"120px"}}>Date</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                {props.calendar && props.calendar.map(e => <tr key={e.id}>
                    <td>{ToFullDateFormat(e.at_date)}</td>
                    <td>
                        <p
                            dangerouslySetInnerHTML={{
                                __html:ParseStyle(e.description)
                            }}
                        />
                    </td>
                </tr>)}
                </tbody>
            </HTMLTable>
        </LazyLoader>
    );
}

export default CalendarRedux(Calendar)
