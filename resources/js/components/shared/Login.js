import React,{useState, useRef, useEffect} from 'react';
import Button from "../forms/Button";
import Alert from "../forms/Alert";
import {LoginRedux} from "../../data/connect";
import {Card, Icon, InputGroup, Tooltip, Divider} from '@blueprintjs/core'
import {Intent} from "@blueprintjs/core/lib/cjs/common/intent";
import {Link, Redirect} from "react-router-dom";
const Login = (props) => {
    const [showPassword,setShowPassword] = useState(false)
    const [redirect,setRedirect] = useState(null)
    const usernameRef = useRef("")
    const passwordRef = useRef("");
    const lockButton = (
        <Tooltip content={`${showPassword ? "Cacher" : "Montrer"} le mot de passe`}>
            <Button
                icon={showPassword ? "eye-open" : "eye-off"}
                intent={Intent.WARNING}
                minimal={true}
                onClick={()=>setShowPassword(!showPassword)}
            />
        </Tooltip>
    );
    const onSubmit = (e)=>{
        e.preventDefault()
        let username = usernameRef.current.value
        let password = passwordRef.current.value
        if(username.length > 0 && password.length>0)
        {
            props.login(username,password)
        }
    }
    return (
        <div>
            {redirect}
            <Card>
                <div className="row justify-content-center pb-3">
                    <Icon iconSize={64} icon={"user"}/>
                </div>
                <form onSubmit={onSubmit} action="#">
                    <InputGroup
                        leftIcon={"user"}
                        placeholder={"Email"}
                        inputRef={usernameRef}
                        type={"text"}
                        name={"email"}
                    />
                    <br/>
                    <InputGroup
                        leftIcon={"lock"}
                        placeholder="Mot de passe"
                        rightElement={lockButton}
                        type={showPassword ? "text" : "password"}
                        inputRef={passwordRef}
                    />
                    <br/>
                    <div  className="row align-items-end flex-column login-bottom-section">
                        <Button type={"submit"} loading_id={"loading_id"} onClick={onSubmit} icon={"key"} intent={"primary"}>S'identifier</Button>
                        <Link to={"/reset"}>Mot de passe oublié?</Link>
                        <Divider className="w-100"/>
                        <Link to={"/signup"} >Créer un compte PFE</Link>
                    </div>
                    <div className="row">
                        <Alert message_id={"login_message_id"}/>
                    </div>
                </form>
            </Card>
        </div>
    );
}

export default LoginRedux(Login)
