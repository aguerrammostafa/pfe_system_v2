import React, {useEffect, useState} from 'react';
import {Callout, HTMLTable, Icon, Intent} from "@blueprintjs/core";
import {FiliereListRedux} from "../../data/connect";
import Button from "../forms/Button";
import PropTypes from "prop-types"
import {Filieres, FilieresTitle} from "../../config/config";
import LazyLoader from "../layouts/LazyLoader";


/**
 * @param filiere, on of gi or gtr
 * */
const FiliereList = FiliereListRedux(({filiere, ...props}) => {
    useEffect(() => {
    }, [props.list])
    return (
        <div className={"row w-100"}>
            <Callout className="row justify-content-center">
                {FilieresTitle[filiere]}
            </Callout>
            <LazyLoader className="w-100" loading_id={"init_students_list_loading"}>
                <HTMLTable striped className="w-100">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Matr</th>
                        <th>Membres</th>
                        <th>Encadrant Interne</th>
                        <th>Sujet et ville</th>
                        {/*<th>Rapport</th>*/}
                    </tr>
                    </thead>
                    <tbody>
                    {
                        props.list.length > 0 && props.list.map((e, index) => {
                            if (e.filiere.toUpperCase() === filiere) {
                                return <tr key={e.id}>
                                    <td>{index + 1}</td>
                                    <td>{e.filiere}-{e.id}</td>
                                    <td>{e.etudiants && e.etudiants.map(et => (
                                        <p key={et.id}>{et.nom} {et.prenom}</p>
                                    ))}</td>
                                    <td>{
                                        e.encadrant && <p>{e.encadrant.nom} {e.encadrant.prenom}</p>
                                    }</td>
                                    <td><b>{e.sujet}</b>{e.ville_entreprise &&
                                    <span className={"pl-1"}>[{e.ville_entreprise}]</span>} </td>
                                    {/*<td>
                                        {
                                            e.active_report && e.active_report.id ?
                                                <a href={`/api/report/download/${e.active_report.find}`}
                                                   title={e.active_report.title} target="_blank">
                                                    <Button fill intent={Intent.PRIMARY} icon={"download"}/>
                                                </a>
                                                : <Icon icon={"clock"}/>}
                                    </td>*/}
                                </tr>
                            }
                            return null
                        })
                    }
                    </tbody>
                </HTMLTable>
            </LazyLoader>
        </div>
);
})

FiliereList.propTypes = {
    filiere:PropTypes.oneOf([Filieres.gi,Filieres.gtr]).isRequired
}
export default FiliereList
