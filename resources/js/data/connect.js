import { connect } from "react-redux";
import Types from "../data/types";
import { push } from "connected-react-router";
import { Filieres } from "../config/config";
import { call } from "redux-saga/effects";

const listCalendar = () => ({
    type: Types.api.get,
    request: {
        url: "/calendar"
    },
    loading: "calendar_list",
    onSuccess: [Types.calendar.set]
});
const listJury = () => ({
    type: Types.api.get,
    request: {
        url: "/admin/jury/list"
    },
    loading: "jury_list",
    onSuccess: [Types.jury.init]
})
const fetchMyPdef = () => ({
    type: Types.api.get,
    request: {
        url: "/form",
        headers: {
            Accept: "text/html"
        }
    },
    loading: "fetch_my_document",
    onSuccess: [Types.my_document.set]
});
const init_students_list = () => ({
    type: Types.api.get,
    request: {
        url: "/etudiant/"
    },
    loading: "init_students_list_loading",
    onSuccess: [Types.students_list.set]
});
const getEncadrantList = () => ({
    type: Types.api.get,
    request: {
        url: "/admin/encadrant"
    },
    loading: "get_encd_list",
    onSuccess: [Types.encadrant.set]
});

let initReportsList = () => ({
    type: Types.api.get,
    request: {
        url: "/report/"
    },
    loading: "get_reports_list",
    onSuccess: [Types.reports_list.set]
});
let toastMessage = (message, message_id, type = "success") => ({
    type: Types.message.toast,
    payload: {
        id: message_id,
        type,
        message
    }
});

export const AppRedux = connect(
    state => ({
        auth: state.auth,
        router: state.router,
        students_list: state.students_list
    }),
    {
        init_students_list: taskIncrement => ({
            ...init_students_list(),
            onFinish: [taskIncrement]
        }),
        bootstrap: () => ({
            type: Types.auth.set
        }),
        checkToken: (callback, admin = false) => ({
            type: Types.api.post,
            request: {
                url: admin ? "/admin/auth/me" : "/auth/me"
            },
            onError: [Types.auth.logout]
        }),
        init_settings: taskIncrement => ({
            type: Types.api.get,
            request: {
                url: "/settings"
            },
            onSuccess: [Types.settings.init],
            onFinish: [taskIncrement]
        }),
        init_calendar: taskIncrement => ({
            ...listCalendar(),
            onFinish: [taskIncrement]
        })
    }
);
export const FiliereListRedux = connect(
    state => ({
        list: state.students_list
    }),
    {}
);

export const LoginRedux = connect(state => ({}), {
    login: (username, password) => ({
        type: Types.api.post,
        request: {
            url: "/auth/login",
            data: {
                email: username,
                password
            }
        },
        message: "login_message_id",
        loading: "loading_id",
        onSuccess: [Types.auth.login],
        onError: ["error_action"]
    })
});
export const AlertRedux = connect(
    state => ({
        message: state.message
    }),
    {
        unsetMessage: id => ({
            type: Types.message.unset,
            payload: id
        })
    }
);
export const ButtonRedux = connect(state => ({
    loading: state.loading
}));

export const DashboardRedux = connect(
    state => ({
        settings: state.settings,
        reports_list: state.reports_list
    }),
    {
        logout: () => ({
            type: Types.auth.logout
        }),
        message: toastMessage
    }
);

export const BreadCrumbRedux = connect(state => ({}), {
    push: path => push(path)
});

export const EditFormRedux = connect(
    state => ({
        editform: state.editform,
        settings: state.settings
    }),
    {
        updateProps: (path, value, index) => ({
            type: Types.editForm.update,
            payload: {
                path,
                value,
                index
            }
        }),
        createIndex: index => ({
            type: Types.editForm.createIndex,
            payload: {
                index
            }
        }),
        clearFields: () => ({
            type: Types.editForm.clear
        }),
        initData: () => ({
            type: Types.api.get,
            request: {
                url: "/formIndex"
            },
            onSuccess: [Types.editForm.set]
        }),
        update_form_data: data => {
            let d = { ...data };
            d.list = [...d.list.slice(0, d.total_members)];
            return {
                type: Types.api.put,
                request: {
                    url: "/formPut",
                    data: d
                },
                message: "editform_message",
                loading: "editform_loading",
                onSuccess: [
                    Types.editForm.set,
                    Types.message.editFormSuccess,
                    init_students_list(),
                    fetchMyPdef()
                ],
                onError: [
                    toastMessage(
                        "Il y a quelques erreurs veuillez les vérifier en bas de page",
                        "editform_message_toast",
                        "danger"
                    )
                ]
            };
        }
    }
);

export const SignupRedux = connect(
    state => ({
        signup: state.signup,
        settings: state.settings
    }),
    {
        updateProps: (path, value, index) => ({
            type: Types.signup.update,
            payload: {
                path,
                value,
                index
            }
        }),
        createIndex: index => ({
            type: Types.signup.createIndex,
            payload: {
                index
            }
        }),
        clearFields: () => ({
            type: Types.signup.clear
        }),
        signup_form: data => {
            let d = { ...data };
            d.list = [...d.list.slice(0, d.total_members)];
            return {
                type: Types.api.post,
                request: {
                    url: "/signup",
                    data: d
                },
                message: "signup_message",
                loading: "signup_loading",
                onSuccess: [Types.signup.clear]
            };
        }
    }
);

export const ReportRedux = connect(
    state => ({
        reports_list: state.reports_list
    }),
    {
        initList: initReportsList,
        setErrorMessage: errors => {
            return {
                type: Types.message.toast,
                payload: {
                    id: "report_upload",
                    message: errors,
                    type: "danger"
                }
            };
        },
        uploadReport: (file, isFinal, callback) => ({
            type: Types.api.file,
            request: {
                url: "/report",
                data: {
                    file,
                    isFinal
                }
            },
            loading: "upload_report",
            message: "upload_report_message",
            onSuccess: [
                initReportsList(),
                () => {
                    callback(null);
                },
                init_students_list()
            ]
        }),
        deleteReport: id => ({
            type: Types.api.delete,
            request: {
                url: "report",
                data: {
                    id
                }
            },
            loading: `delete_report_${id}`,
            onSuccess: [
                {
                    type: Types.reports_list.remove,
                    payload: id
                },
                toastMessage("Votre rapport a été supprimé", "report_delete"),
                init_students_list()
            ]
        })
    }
);

export const ResetPasswordRedux = connect(state => ({}), {
    resetPassword: (username, callback) => ({
        type: Types.api.post,
        request: {
            url: "/reset",
            data: {
                email: username
            }
        },
        message: "reset_password",
        loading: "reset_loading",
        onSuccess: [callback]
    })
});
export const RecoverPasswordRedux = connect(state => ({}), {
    checkToken: (token, email, successCallBack, errorCallBack) => ({
        type: Types.api.get,
        request: {
            url: `/reset/${token}/${email}`
        },
        onSuccess: [successCallBack],
        onError: [errorCallBack]
    }),
    updatePassword: (token, email, password, rPassword, callback) => ({
        type: Types.api.put,
        request: {
            url: `/reset/`,
            data: {
                token,
                email,
                password,
                r_password: rPassword
            }
        },
        message: "reset_password_m",
        loading: "reset_password",
        onSuccess: [callback]
    })
});
export const AdminAuthRedux = connect(state => ({}), {
    login: (username, password) => ({
        type: Types.api.post,
        request: {
            url: "/admin/auth",
            data: {
                username,
                password
            }
        },
        message: "admin_auth",
        loading: "admin_auth",
        onSuccess: [Types.auth.login]
    })
});
export const AdminDashboardRedux = connect(
    state => ({
        auth: state.auth
    }),
    {
        logout: () => ({
            type: Types.auth.logout
        })
    }
);
export const AddSupervisorRedux = connect(state => ({}), {
    addSupervisor: (nom, prenom, gsm, email, login, password, callback) => ({
        type: Types.api.post,
        request: {
            url: "/admin/encadrant",
            data: {
                nom,
                prenom,
                gsm,
                email,
                username: login,
                password
            }
        },
        message: "add_supr",
        loading: "add_supr",
        onSuccess: [
            toastMessage("Encadrant ajouté avec succès", "add_supr"),
            callback,
            getEncadrantList(),
            listJury()
        ]
    }),
    initValues: (callback) => ({
        type: Types.api.get,
        request: {
            url: "/admin/encadrant/me"
        },
        onSuccess: [callback]
    }),
    updateSuperVisor: (nom, prenom, gsm, email, password, fonction, callback) => ({
        type: Types.api.put,
        request: {
            url: "/admin/encadrant/update",
            data: {
                nom,
                prenom,
                gsm,
                email,
                password,
                fonction
            }
        },
        message: "add_supr",
        loading: "add_supr",
        onSuccess: [callback, listJury(), toastMessage("vos données ont été mises à jour avec succès", "add_supr")]
    })
});

export const ListSupervisorsRedux = connect(
    state => ({
        encadrant: state.encadrant
    }),
    {
        getList: getEncadrantList,
        deleteEncd: id => ({
            type: Types.api.delete,
            request: {
                url: `/admin/encadrant/${id}`
            },
            loading: `deleteing${id}`,
            onSuccess: [
                toastMessage("delete_enc", "Supprimé avec succès"),
                getEncadrantList(),
                init_students_list(),
                listJury()
            ]
        }),
        resetPassword: (id, callback) => ({
            type: Types.api.put,
            request: {
                url: `/admin/encadrant/reset_s_password/${id}`
            },
            loading: `enc_p_${id}`,
            onSuccess: [
                callback,
                toastMessage(
                    "Réinitialisation du mot de passe réussie",
                    "delete_enc"
                )
            ]
        })
    }
);

export const AffectSupervisorRedux = connect(
    state => ({
        encadrants: state.encadrant,
        students_list: state.students_list
    }),
    {
        init: getEncadrantList,
        init_s: init_students_list,
        affectEnc: (list, callback) => ({
            type: Types.api.put,
            request: {
                url: "/admin/encadrant/",
                data: {
                    list
                }
            },
            loading: "affect_enc",
            message: "affect_enc",
            onSuccess: [
                toastMessage("Mis à jour avec succés", "affect_enc"),
                callback,
                init_students_list(),
                getEncadrantList(),
                listJury()
            ]
        })
    }
);

export const ListPfEsRedux = connect(state => ({}), {
    init: callback => ({
        type: Types.api.get,
        request: {
            url: "/admin/attestation"
        },
        onSuccess: [callback]
    })
});

export const ListDesPfEsRedux = connect(state => ({}), {});

export const AdminCalendarRedux = connect(
    state => ({
        calendar: state.calendar
    }),
    {
        list: listCalendar,
        addCalendar: (date, description, callback) => ({
            type: Types.api.post,
            request: {
                url: "/admin/calendar",
                data: {
                    at_date: date,
                    description
                }
            },
            message: "add_calendar",
            loading: "add_calendar",
            onSuccess: [callback, listCalendar()]
        }),
        editCalendarAPI: (id, date, description, callback) => ({
            type: Types.api.put,
            request: {
                url: `/admin/calendar/${id}`,
                data: {
                    at_date: date,
                    description
                }
            },
            message: "add_calendar",
            loading: "add_calendar",
            onSuccess: [listCalendar(), callback]
        }),
        deleteCalendar: id => ({
            type: Types.api.delete,
            request: {
                url: `/admin/calendar/${id}`
            },
            loading: `calendar_d_${id}`,
            onSuccess: [
                toastMessage(
                    "Le calendrier a été supprimé avec succès",
                    "calendar_message"
                ),
                listCalendar()
            ]
        })
    }
);
export const CalendarRedux = connect(
    state => ({
        calendar: state.calendar
    }),
    {}
);
export const MyPdfRedux = connect(
    state => ({
        my_document: state.my_document
    }),
    {
        fetch: fetchMyPdef,
        download: () => ({
            type: Types.api.get,
            request: {
                url: "/form/download",
                responseType: "blob",
                headers: {
                    Accept: "application/pdf"
                }
            },
            loading: "pdf_pfe_download",
            onSuccess: [
                payload => {
                    const url = window.URL.createObjectURL(new Blob([payload]));
                    const link = document.createElement("a");
                    link.href = url;
                    link.setAttribute("download", "Ma fiche PFE.pdf");
                    document.body.appendChild(link);
                    link.click();
                }
            ]
        })
    }
);
export const AdminSettings = connect(
    state => ({
        settings: state.settings,
        auth: state.auth
    }),
    {
        save_dates: ({ date_ins_rap, date_pre, date_com_don }) => ({
            type: Types.api.put,
            request: {
                url: "/admin/settings",
                data: {
                    date_com_don,
                    date_ins_rap,
                    date_pre
                }
            },
            loading: "admin_settings",
            onSuccess: [
                toastMessage("Mis à jour avec succés", "admin_settings"),
                Types.settings.reset_version
            ],
            onError: [
                toastMessage(
                    "Veuillez vérifier vos entrées",
                    "admin_settings",
                    "danger"
                )
            ]
        }),
        update_password: (current, new_password, callback) => ({
            type: Types.api.put,
            request: {
                url: "/admin/settings/password",
                data: {
                    current_password: current,
                    new_password
                }
            },
            loading: "admin_settings",
            message: "admin_settings_account",
            onSuccess: [
                toastMessage(
                    "Mis à jour avec succés",
                    "admin_settings_account"
                ),
                callback
            ]
        }),
        update_location: (location, value) => ({
            type: Types.settings.update_location,
            payload: {
                location,
                value
            }
        })
    }
);

export const HomePanelRedux = connect(
    (state) => ({

    }),
    {
        fetch: (callback) => ({
            type: Types.api.get,
            request: {
                url: "/admin/statistics"
            },
            loading: "admin_statistics",
            onSuccess: [callback]
        })
    }
)

export const JuryRedux = connect(
    (state) => ({
        jury: state.jury
    }),
    {
        init: listJury,
        addJury: (title, callback) => ({
            type: Types.api.post,
            request: {
                url: "/admin/jury/create/",
                data: {
                    title
                }
            },
            loading: "add_jury_loading",
            onSuccess: [listJury(), callback, toastMessage("votre jury a été ajouté", "jury_result")]
        }),
        removeEncadrant: (encadrant_id, jury_id) => ({
            type: Types.api.delete,
            request: {
                url: `/admin/jury/removeEnc/${encadrant_id}`
            },
            onSuccess: [listJury(), getEncadrantList()],
            loading: `remove_btn_${encadrant_id}_${jury_id}`
        }),
        removeJury: (jury_id) => ({
            type: Types.api.delete,
            request: {
                url: `/admin/jury/remove/${jury_id}`
            },
            onSuccess: [listJury(), getEncadrantList()],
            loading: `remove_jury_${jury_id}`
        }),
        affectJury: (enc_id, jury_id, callback = () => { }) => ({
            type: Types.api.put,
            request: {
                url: `/admin/jury/assignEncadrant/${enc_id}/${jury_id}`
            },
            loading: "affect_jury_loading",
            onSuccess: [
                listJury(),
                getEncadrantList(),
                toastMessage("votre encadrant a été modifié", "jury_result"),
                callback
            ]
        })
    }
)
export const HomePanelSupervisorRedux = connect(
    (state) => ({

    }),
    {
        init_team: (callback) => ({
            type: Types.api.get,
            request: {
                url: "/admin/encadrant/team"
            },
            onSuccess: [callback],
            loading: "team_jury"
        }),
        init_my_students: (callback) => ({
            type: Types.api.get,
            request: {
                url: "/admin/encadrant/ms"
            },
            loading: "my_students",
            onSuccess: [callback]
        }),
        init_team_students: (callback) => ({
            type: Types.api.get,
            request: {
                url: "/admin/encadrant/teamms"
            },
            loading: "team_students",
            onSuccess: [callback]
        }),
    }
)
export const UserHomePanel = connect(
    (state) => ({

    }),
    {
        init: (callback) => ({
            type: Types.api.get,
            request: {
                url: "/me/encadrant"
            },
            loading: "enc_loading",
            onSuccess: [callback]
        }),
        deleteAccount: (callback) => ({
            type: Types.api.delete,
            request: {
                url: "/me/delete"
            },
            loading: "me_delete",
            onSuccess: [callback]
        }),
        logout: () => ({
            type: Types.auth.logout
        })
    }
)
