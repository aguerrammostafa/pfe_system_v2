import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import sagaMiddleware from './sagas/sagas'
import {connectRouter} from 'connected-react-router'
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly'
import history from "../components/routes/history";
import {routerMiddleware} from 'connected-react-router'

const reducers = {
        router: connectRouter(history)
    },
    initial = {}


//Add new reducers here
const hocs = {
    auth: require("./hocs/auth"),
    message: require("./hocs/message"),
    loading: require("./hocs/loading"),
    students_list: require("./hocs/students_list"),
    signup: require("./hocs/signup"),
    editform: require("./hocs/editform"),
    reports_list: require("./hocs/reports_list"),
    encadrant: require("./hocs/encadrant"),
    calendar: require("./hocs/calendar"),
    my_document: require("./hocs/my_document"),
    settings:require("./hocs/settings"),
    jury:require("./hocs/jury"),
}

for (let hoc in hocs) {
    reducers[hoc] = hocs[hoc].reducer
    initial[hoc] = hocs[hoc].initial
}
const composeEnhancers = composeWithDevTools({})
const Store = window.store = createStore(
    combineReducers(reducers),
    initial,
    composeEnhancers(
        applyMiddleware(
            sagaMiddleware,
            routerMiddleware(history)
        )
    )
);
export default Store
