import Types from "../types"
import config, {Filieres} from "../../config/config";

export const initial = {
    total_members: 1,
    version: 0,
    filiere: Filieres.gi,
    username: "",
    password: "",
    r_password: "",
    list: [
        {
            nom: "",
            prenom: "",
            email: "",
            gsm: "",
        }
    ]
}
export const reducer = (state = initial, action) => {
    switch (action.type) {
        case Types.signup.clear:
            return {
                ...initial,
                version: 0,
                total_members: 1
            }
        case Types.signup.createIndex:
            let {index: Val} = action.payload
            if (state.total_members <= 1) {
                Val = 1
            } else if (state.total_members >= config.MAX_MEMBERS) {
                Val = config.MAX_MEMBERS
            }
            if(state.list[Val - 1])
            {
                return {
                    ...state,
                    version: ++state.version,
                    total_members: Val,
                }
            }
            else{
                return {
                    ...state,
                    version: ++state.version,
                    total_members: Val,
                    list: [
                        ...state.list,
                        {
                            nom: "",
                            prenom: "",
                            email: "",
                            gsm: "",
                            ...state.list[Val - 1]
                        }
                    ]
                }
            }
        case Types.signup.update:
            let {value, index, path} = action.payload
            if (action.payload.path === "total_members") {
                return {
                    ...state
                }
            }
            if (index === undefined) {
                return {
                    ...state,
                    version: ++state.version,
                    [path]: value
                }
            } else {
                let nList = [...state.list];
                nList[index]  = {
                    ...nList[index],
                    [path]: value
                };
                return {
                    ...state,
                    version: ++state.version,
                    list: [
                        ...nList
                    ]

                }
            }
        default:
            return state
    }
}
