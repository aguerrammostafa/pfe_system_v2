import Type from '../types'
import _ from 'lodash'
export const initial = {}

export const reducer = (state = initial, action) => {
    switch (action.type) {
        case Type.loading.set:
            return {
                ...state,
                [action.payload]: true
            }

        case Type.loading.unset:
            let newState = {...state}
            delete newState[action.payload]
            return {
                ...newState
            }
        default:
            return state;
    }
}
