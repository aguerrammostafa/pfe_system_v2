import Types from '../types'
export const initial = []

export const reducer = (state = initial,action)=>{
    switch(action.type)
    {
        case Types.calendar.set:
            return [
                ...action.payload
            ]
        default: return state;
    }
}
