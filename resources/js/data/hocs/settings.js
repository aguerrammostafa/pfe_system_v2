import Types from "../types"
export const initial = {
    version:1,
    date_pre:Date.now(),
    date_com_don:Date.now(),
    date_ins_rap:Date.now()
}

export const reducer = (state = initial,action) =>{
    switch(action.type)
    {
        case Types.settings.update_location:
            return {
                ...state,
                [action.payload.location]:action.payload.value,
                version:state.version+1
            }

        case Types.settings.reset_version:
            return {
                ...state,
                version:1
            }
        case Types.settings.init:
            return {
                ...action.payload
            }
        default:return state;
    }
}
