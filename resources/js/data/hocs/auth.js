import Type from '../types'

export const initial = {}
export const reducer = (state = initial, action) => {
    switch (action.type) {
        case Type.auth.login:
            window.localStorage.setItem("token", action.payload.access_token)
            window.auth = action.payload
            if (action.payload.admin) {
                window.localStorage.setItem("admin", action.payload.admin)
            }
            return {
                ...state,
                ...action.payload
            }

        case Type.auth.logout:
            window.localStorage.removeItem("token");
            window.localStorage.removeItem("admin");
            let isAdmin = window.auth.admin == false || window.auth.admin == true
            window.location.href = isAdmin?"/admin":"/"
            window.auth = undefined;
            return {
                ...state
            }
        case Type.auth.unset:
            window.localStorage.removeItem("token")
            window.localStorage.removeItem("admin")
            window.auth = undefined;
            return {}
        case Type.auth.set:
            let key = window.localStorage.getItem("admin");
            let st = {
                ...state,
                access_token: window.localStorage.getItem("token"),
                ...(key != null?{admin:key}:{})
            };

            window.auth = st;

            return st;
        default:
            return state
    }
}
