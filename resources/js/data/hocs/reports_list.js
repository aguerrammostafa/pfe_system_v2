import Types from "../types"
export const initial = []

export const reducer = (state = initial,action) =>{
    switch(action.type)
    {
        case Types.reports_list.set:
            return [...action.payload]
        case Types.reports_list.remove:
            return [
                ...state.filter(e=>e.id !== action.payload)
            ]
        default:return state;
    }
};
