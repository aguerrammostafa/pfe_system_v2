import Types from '../types'

export const initial = {
    html:null
}

export const reducer = (state=initial,action) =>{
    switch(action.type)
    {
        case Types.my_document.set:
            return {
                ...state,
                html:action.payload
            };
        default:return state
    }
}
