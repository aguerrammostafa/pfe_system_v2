import Types from '../types'

export const initial = {}

export const reducer = (state = initial,action) =>{
    switch (action.type) {
        case Types.message.set:
            return {
                ...state,
                [action.payload.id]:{
                    message:action.payload.message,
                    type:action.payload.type
                }
            }
        case Types.message.toast:
            return {
                ...state,
                [action.payload.id]:{
                    message:action.payload.message,
                    type:`toast_${action.payload.type}`
                }
            }
        case Types.message.unset:
            let newState = {...state}
            delete newState[action.payload]
            return {
                ...newState
            }
        case Types.message.editFormSuccess:
            return {
                ...state,
                editform_message:{
                    message:"vos données ont été mises à jour avec succès",
                    type:"toast_success"
                }
            }
        default: return state
    }
}
