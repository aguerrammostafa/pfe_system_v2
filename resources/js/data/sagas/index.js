import {all, takeLatest, takeEvery, throttle, cancel, fork, take } from 'redux-saga/effects'
import {
    httpGet,
    httpDelete,
    httpPost,
    httpPut, httpFile
} from './actions'
import Types from '../types'
function * apiListener(){
    yield all([
        takeEvery(Types.api.get, httpGet),
        takeEvery(Types.api.post, httpPost),
        takeEvery(Types.api.put, httpPut),
        takeEvery(Types.api.delete, httpDelete),
        takeEvery(Types.api.file, httpFile),
    ])
}
function * loginHandler() {

}
export default function* (){
    yield all([
        apiListener()
    ])
}
