import { call, put, select, take } from "redux-saga/effects";
import Types from "../types";
import axios from "../../utils/axios";
import _ from "lodash";

function* loader(id, action = "set") {
    if (id) {
        yield put({
            type: Types.loading[action],
            payload: id
        });
    }
}

function* unsetMessage(id) {
    if (id) {
        yield put({
            type: Types.message.unset,
            payload: id
        });
    }
}

function* message(id, message, type = "success") {
    if (id) {
        yield put({
            type: Types.message.set,
            payload: {
                id,
                message,
                type
            }
        });
    }
}

function* callActionHandler(action, data) {
    if (action) {
        for (let e in action) {
            if (_.isFunction(action[e])) {
                yield call(action[e], data);
            } else if (_.isObject(action[e])) {
                yield put(action[e]);
            } else if (_.isString(action[e])) {
                yield put({
                    type: action[e],
                    payload: data
                });
            }
        }
    }
}

function* ActionHandler(rest, response) {
    try {
        let { onSuccess, onError, onFinish } = rest;
        let { data, status, headers } = response;
        let contentType = headers && headers["content-type"];
        if (onSuccess) {
            if (!_.isArray(onSuccess))
                throw new Error(
                    "onSuccess handler must be an array of actions or reducers"
                );
        }
        if (onError) {
            if (!_.isArray(onError))
                throw new Error(
                    "onError handler must be an array of actions or reducers"
                );
        }

        if (onFinish) {
            if (!_.isArray(onFinish))
                throw new Error(
                    "onFinish handler must be an array of actions or reducers"
                );
        }
        yield callActionHandler(onFinish, data);
        if (status === 200) {
            yield callActionHandler(onSuccess, data);
            if (data.message) yield message(rest.message, data.message);
        } else if (status === 400) {
            yield callActionHandler(onError, data);
            if (data.message || data.error)
                yield message(
                    rest.message,
                    data.message || data.error,
                    "error"
                );
        } else if (
            status === 401 &&
            ["Unauthenticated.", "TokenChanged."].includes(data.message)
        ) {
            yield put({
                type: Types.auth.logout
            });
        } else if (status === 401) {
            yield callActionHandler(onError, data);
            if (data.message || data.error)
                yield message(
                    rest.message,
                    data.message || data.error,
                    "error"
                );
        } else if (status === 422) {
            yield callActionHandler(onError, data);
            if (data.errors && _.isObject(data.errors)) {
                let _message = "";
                for (let err in data.errors) {
                    _message += `<b>${err}</b><br/>`;
                    if (_.isArray(data.errors[err])) {
                        for (let el in data.errors[err]) {
                            _message += `- ${data.errors[err][el]} <br/>`;
                        }
                    }
                }
                if (_message.length > 0) {
                    yield message(rest.message, _message, "error");
                }
            }
        } else if (status == 500) {
            yield message(
                rest.message,
                "Erreur inattendue, si cela continue, veuillez nous contacter",
                "error"
            );
        }
        /*return yield put.resolve({
                type: Types.auth.logout,message: response.data.message
            });*/
    } catch (ex) {
        console.error(ex);
    }
}

function* httpCall(config, rest) {
    try {
        yield loader(rest.loading, "set");
        yield unsetMessage(rest.message);
        let response = null;
        if (rest.isFile) {
            let {data:{file,...restData}, url} = config;
            if (file) {
                let form = new FormData();
                form.append("file", file, file.name);
                for (let key of Object.keys(restData))
                {
                    form.append(key, restData[key]);
                }
                response = yield call(axios.post, url, form);
            }
        } else {
            response = yield call(axios, config);
        }
        if (response !== null) yield ActionHandler(rest, response);
    } catch (ex) {
        if (process.env.NODE_ENV == "development") {
            console.error(ex);
        }
        yield ActionHandler(rest, ex.response, "error");
    } finally {
        yield loader(rest.loading, "unset");
    }
}

export function* httpGet({ request, ...rest }) {
    yield call(httpCall, request, rest);
}

export function* httpPost({ request, ...rest }) {
    yield call(httpCall, { ...request, method: "POST" }, rest);
}

export function* httpFile({ request, ...rest }) {
    yield call(httpCall, { ...request }, { isFile: true, ...rest });
}

export function* httpDelete({ request, ...rest }) {
    yield call(httpCall, { ...request, method: "DELETE" }, rest);
}

export function* httpPut({ request, ...rest }) {
    yield call(httpCall, { ...request, method: "PUT" }, rest);
}
