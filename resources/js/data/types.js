export default  {
    auth : {
        login : "@@auth:login",
        login_admin : "@@auth:login_admin",
        logout: "@@auth:logout",
        set:"@@auth:set",
        unset:"@@auth:unset",
    },
    api : {
        get: "@@api:get",
        post: "@@api:post",
        put: "@@api:put",
        delete: "@@api:delete",
        file:"@@api:file"
    },
    message: {
        unset: "@@message:unset",
        set: "@@message:set",
        editFormSuccess:"@@message:editFormSuccess",
        toast:"@@message:toast"
    },
    loading: {
        set: "@@loading:set",
        unset: "@@loading:unset",
        clear:"@@loading:clear"
    },
    students_list:{
        set:"@@students_list:set"
    },
    signup:{
        update:"@@signup:update",
        createIndex:"@@signup:createIndex",
        clear:"@@signup:clear"
    },
    editForm:{
        update:"@@editForm:update",
        set:"@@editForm:set",
        createIndex:"@@editForm:createIndex",
        clear:"@@editForm:clear"
    },
    reports_list:{
        set:"@@reports_list:set",
        remove:"@@reports_list:remove"
    },
    encadrant:{
        set:"@@encadrant:list"
    },
    calendar:{
        set:"@@calendar:set"
    },
    my_document:{
        set:"@@my_document:set"
    },
    settings:{
        init:"@@settings:init",
        update_location:"@@settings:update_location",
        reset_version:"@@settings:reset_version"
    },
    jury:{
        init:"@@jury:init"
    }
}
