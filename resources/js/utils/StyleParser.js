import moment from "moment";

export const ParseStyle = (text) => {
    text = text.replace(/(<([^>]+)>)/ig, "");
    let openBold = false;
    let openItalic = false;
    let openDel = false;
    let newText = "";
    for (let c of text) {
        if (c === '*' && !openBold) {
            newText += "<b>"
            openBold = true
        } else if (c === '*' && openBold) {
            newText += "</b>"
            openBold = false;
        } else if (c === '_' && !openItalic) {
            newText += "<i>"
            openItalic = true;
        } else if (c === '_' && openItalic) {
            newText += "</i>"
            openItalic = false;
        } else if (c === '~' && !openDel) {
            newText += "<del>"
            openDel = true;
        } else if (c === '~' && openDel) {
            newText += "</del>"
            openDel = false;
        } else {
            newText += c;
        }
    }
    return newText
}
export const ToFullDateFormat = (dateString,parse=false) => {
    if(parse)
    {
        let date = new Date(dateString)
        dateString = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}`
    }
    moment.locale("fr")
    let res = moment(dateString).format("LLLL");
    res = res.slice(0, res.length - 5)
    return res.charAt(0).toUpperCase() + res.slice(1)
}
