/**
 * @param {number|string} date1
 * @param {number|string} date2, leave null of the date is today
 * @returns the difference of date1 - date2 (Today if not set), or returns 0 if date2 is the bigger
 */
export const CompareDates = (date1, date2 = null) => {
    if (!date2) {
        let d = new Date();
        date2 = new Date(`${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()}`).getTime();
    }
    if (typeof date1 == "string") date1 = new Date(date1).getTime();
    let result = (date1 - date2) / (1000 * 60 * 60 * 24);
    return Math.floor(result);
};
