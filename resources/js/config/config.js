export default
{
    BASE_URL:"/api/",
    MAX_MEMBERS:5,
    MAXIMUM_FILE_SIZE:5 * 1000000
}

export const Filieres = {
    "gtr":"GTR",
    "gi":"GI",
}

export const PFETypes = [
    {value: 0, label: "Non défini"},
    {value: 1, label: "Oui"},
    {value: 2, label: "Non"},
]

export const FilieresTitle = {
    [Filieres.gtr]:"Génie Télécom et Réseaux",
    [Filieres.gi]:"Génie Informatique"
}
