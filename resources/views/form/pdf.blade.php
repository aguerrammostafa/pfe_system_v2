<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
    <style>
        .w-100 {
            width: 100%;
        }


        p {
            font-family: verdana;
            font-size: 10px;
        }

        .head {
            font-family: verdana;
            font-size: 10px;
        }

        .title {
            font-family: verdana;
            font-size: 12px;
            font-weight: bold;
            color: #636161;
        }

        .normal-text {
            font-family: verdana;
            font-size: 11px;
        }

        .center {
            margin-left: auto;
            margin-right: auto;
        }

        .padding-50 {
            padding: 50px;
        }

        .padding-10 {
            padding: 10px;
        }

        th, p {
            color: #777777;
        }

        p > b {
            color: black;
        }

        th, td {
            text-align: center;
        }
    </style>
</head>
<body>
<table class="w-100 center">
    <tr>
        <td>
            <img src="data:image/png;base64,{{$logo}}" width="70" height="60"/>
        </td>
    </tr>
    <tr>
        <td>
            <p class="head">Université Cadi Ayyad</p>
            <p class="head">ENSA Safi</p>
            <p class="head">Département Informatique, Réseaux et Télécommunications</p>
            <p class="head">Fiche PFE</p>
            <p class="head">{{$filiere}}</p>
        </td>
    </tr>
</table>
<div class="padding-50 normal-text">
    <h2 class="title">Membres du groupe PFE</h2>
    <hr>
    <table class="w-100 border">
        <thead>
        <tr>
            <th class="padding-10">Nom</th>
            <th class="padding-10">Prenom</th>
            <th class="padding-10">Email</th>
            <th class="padding-10">GSM</th>
        </tr>
        </thead>
        <tbody>
        @foreach($etudiants as $et)
            <tr>
                <td class="padding-10">{{$et['nom']}}</td>
                <td class="padding-10">{{$et['prenom']}}</td>
                <td class="padding-10">{{$et['email']}}</td>
                <td class="padding-10">{{$et['gsm']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <br/>
    <h2 class="title">Thème du PFE</h2>
    <hr>
    <p class="normal-text">Intitulé du PFE: <b>{{$sujet}}</b></p>
    <p class="normal-text">Description du PFE: <b>{{$desc}}</b></p>
    <p class="normal-text">Mots-clés: <b>{{$mots_cle}}</b></p>
    <p class="normal-text">PFE pré-embauche:@switch($pre_embauche)
            @case(0)
            <b>Pas Défini</b>
            @break
            @case(1)
            <b>Oui</b>
            @break
            @case(2)
            <b>Non</b>
            @break
        @endswitch
    </p>
    <p class="normal-text">Date début de stage: <b>{{$debut_stage}}</b></p>
    <p class="normal-text">Durée de stage: <b>{{$duree_stage}}</b> Jours ({{$duree_stage/30}} mois) </p>


    <br/>
    <h2 class="title">Entreprise & Encadrement externe</h2>
    <hr>
    <p class="normal-text">Entreprise & Département: <b>{{$entreprise}}</b></p>
    <p class="normal-text"> Coordonnés et Web:<b>{{$coordonnes_entreprise}}</b></p>
    <p class="normal-text">Ville:<b>{{$ville_entreprise}}</b></p>
    <p class="normal-text">Encadrant(s) externe(s):<b>{{$nom_encadrant_ext}}</b></p>
    <p class="normal-text">Fonction de l'encadrant:<b>{{$fonction_encadrant_ext}}</b></p>
    <p class="normal-text">Téléphone / GSM:<b>{{$gsm_encadrant_ext}}</b></p>
    <p class="normal-text">Email:<b>{{$email_encadrant_ext}}</b></p>


    <br/>
    <hr>
    <h2 class="title">Encadrement pédagogique</h2>
    @if(isset($encadrant))
        <p class="normal-text">Encadrant pédagogique: <b>{{$encadrant['nom']}} {{$encadrant['prenom']}}</b></p>
        <p class="normal-text">Email de l'Encadrant pédagogique:<b>{{$encadrant['email']}}</b></p>
        <p class="normal-text">Date d'enregistrement :<b>{{substr($created_at,0,strlen($created_at)-8)}}</b></p>
    @else
        <p class="normal-text">Ll n'y a pas encore d'encadrant</p>
    @endif
</div>


</body>
</html>
