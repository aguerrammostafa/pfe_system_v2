<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
    <style>
        .row {
            display: flex;
            width: 100%;
        }

        .center {
            justify-content: center;

        }

        table{
            width: 100%;
            border-collapse: collapse;

        }
        td {
            border: 1px solid black;
            width: 100px;
            height: 50px;
            padding: 12px 5px;
        }

    </style>
</head>
<body>
<div class="row center">
    <div >
        <h1 align="center">Attestation</h1>
        <br>
        Je soussigné Monsieur BOUARIFI Walid, Chef du département Informatique, Réseaux et Télécommunications, atteste par la présente,
        que Monsieur {{$data['prof_name']}} a assuré l'encadrement des Projets de Fin d'Etudes suivants :

        <table width="100%">
            <tbody>
            @for($i=0;$i<count($data)-1;$i++)
                <tr>
                    <td>
                        {{$data[$i]['year']-1}}-{{$data[$i]['year']}}
                    </td>
                    <td>
                        @foreach($data[$i]['data'] as $d)
                            @foreach($d["etudiants"] as $et)
                                <p>{{$et["nom"]}} {{$et["prenom"]}}</p>
                            @endforeach
                        @endforeach
                    </td>
                    <td>
                        @foreach($data[$i]['data'] as $d)
                            <p>{{$d["sujet"]}}</p>
                        @endforeach
                    </td>
                </tr>
            @endfor
            </tbody>
        </table>
        <p>En foi de quoi, la pésente attestation est délivrée à l'intéressé, suite à sa demande, pour servir et valoir ce que de droit.</p>
        <div style="display: flex;justify-content: end;">
            Fait à Safi, le {{date("d-m-Y")}}
        </div>
        <h3 align="center">Signature</h3>
    </div>
</div>
</body>
</html>
