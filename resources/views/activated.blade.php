<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{asset("css/app.css")}}" rel="stylesheet"/>
    <title>{{config("app.name")}}</title>
</head>
<body>
    <div class="row vh-100 justify-content-center">
        <h4>Votre compte a été activé</h4>
        <script>
            setTimeout(()=>{
                location.href = "/";
            },2000)
        </script>
    </div>
</body>
</html>
