@component("mail::message")

    Vous avez une demande de réinitialisation de mot de passe, veuillez suivre le lien ci-dessous pour réinitialiser.
    @component('mail::button', ['url' => $url])
        Réinitialiser
    @endcomponent
    @include("mails.emailfooter")
@endcomponent
