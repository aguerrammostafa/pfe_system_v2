@component("mail::message")

    Vous devez valider votre compte pour continuer, cliquez sur le lien ci-dessous pour activer votre compte
    @component('mail::button', ['url' => $url])
        Activer
    @endcomponent
    @include("mails.emailfooter")
@endcomponent
